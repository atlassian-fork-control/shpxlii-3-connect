const path = require('path');
const request = require('request');
module.exports = function(app, addon) {
  const devEnv = app.get('env') == 'development';
  const addonKey = 'jira-boards-on-tv';

  const authenticate = () => {
    if (!devEnv) {
      return addon.authenticate();
    }
    return (req, res, next) => next();
  };

  const uuid = () => {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }

    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  };

  const store = {
    baseUrls: {
      'bc-ac101-mp.atlassian.net': {
        clientKey: '90fb2c8f-799f-335b-a688-95bc08eb2480'
      }
    },
    tokens: {
    }
  };

  const validateAuth = (cookie, baseUrl, boardId) => {
    return new Promise((resolve) => {
      const options = {
        url: `https://${baseUrl}/rest/agile/1.0/board/${boardId}`,
        headers: {
          'Cookie': cookie
        }
      };
      request(options, (err, res, body) => {
        if (!err && res.statusCode === 200 && JSON.parse(body).id === boardId) {
          resolve(true);
        } else {
          resolve(false);
        }
      });
    });
  };

  const generateToken = (baseUrl, boardId) => {
    const token = uuid();
    store.tokens[token] = {
      baseUrl,
      boardId,
      clientKey: store.baseUrls[baseUrl].clientKey
    };
    return token;
  };

  app.get('/receiver', (req, res) => res.sendFile(path.join(__dirname + '/../receiver.html')));

  // Root route. This route will serve the `atlassian-connect.json` unless the
  // documentation url inside `atlassian-connect.json` is set
  app.get('/', (req, res) => {
    res.format({
      // If the request content-type is text-html, it will decide which to serve up
      'text/html': function() {
        res.redirect('/atlassian-connect.json');
      },
      // This logic is here to make sure that the `atlassian-connect.json` is always
      // served up when requested by the host
      'application/json': function() {
        res.redirect('/atlassian-connect.json');
      }
    });
  });

  app.get('/healthcheck', (req, res) => {
    res.send('OK');
  });

  [
    '/project-tv-view',
    '/sprint-tv-view',
  ].forEach((reactRoute) => {
    app.get(reactRoute, authenticate(), (req, res) => {
      res.render('index');
    });
  });

  app.post('/setup-cast', (req, res) => {
    const baseUrl = req.body.baseUrl.replace('https://', '').replace('http://', '');
    const boardId = req.body.boardId;
    const cookie = req.body.cookie;

    validateAuth(cookie, baseUrl, boardId)
      .then((valid) => {
        if (!valid) {
          res.status(400).json({
            error: 'Could not generate URL. Check that you have permission to view this board.'
          });
        } else {
          const token = generateToken(baseUrl, boardId);
          const payload = {
            url: `${req.protocol}://${req.hostname}/cast?token=${token}`
          };
          res.json(payload);
        }
      });
  });

  app.get('/cast', (req, res) => {
    const token = req.query.token;
    if (!store.tokens[token]) {
      res.status(400).json({
        error: 'Invalid token'
      });
    } else {
      const boardId = store.tokens[token].boardId;
      res.render('index', { boardId });
    }
  });

  app.get('/project-details', (req, res) => {
    const token = req.query.token;
    const clientKey = store.tokens[token].clientKey;
    const projectId = req.query.projectId;
    const httpClient = addon.httpClient({
      clientKey: clientKey,
      addonKey: addonKey
    });
    httpClient.get(`/rest/api/2/project/${projectId}`, (projectErr, projectRes, projectBody) => {
      if (projectErr) {
        res.status(500).send(projectErr);
      } else {
        res.send(projectBody);
      }
    });
  });

  app.get('/board-details', (req, res) => {
    const token = req.query.token;
    const clientKey = store.tokens[token].clientKey;
    const boardId = req.query.boardId;
    const httpClient = addon.httpClient({
      clientKey: clientKey,
      addonKey: addonKey
    });
    httpClient.get(`/rest/agile/1.0/board/${boardId}`, (boardErr, boardRes, boardBody) => {
      if (boardErr) {
        res.status(500).send(boardErr);
      } else {
        res.send(boardBody);
      }
    });
  });

  app.get('/epics', (req, res) => {
    const token = req.query.token;
    const clientKey = store.tokens[token].clientKey;
    const boardId = req.query.boardId;
    const httpClient = addon.httpClient({
      clientKey: clientKey,
      addonKey: addonKey
    });
    httpClient.get(`/rest/agile/1.0/board/${boardId}/epic`, (epicsErr, epicsRes, epicsBody) => {
      if (epicsErr) {
        res.status(500).send(epicsErr);
      } else {
        res.send(epicsBody);
      }
    });
  });

  app.get('/epic-issues', (req, res) => {
    const token = req.query.token;
    const clientKey = store.tokens[token].clientKey;
    const epicId = req.query.epicId;
    const httpClient = addon.httpClient({
      clientKey: clientKey,
      addonKey: addonKey
    });
    httpClient.get(`/rest/agile/1.0/epic/${epicId}/issue`, (issuesErr, issuesRes, issuesBody) => {
      if (issuesErr) {
        res.status(500).send(issuesErr);
      } else {
        res.send(issuesBody);
      }
    });
  });
};
