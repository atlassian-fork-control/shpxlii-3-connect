#!/bin/bash

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
PARENT_DIR="$(dirname "${SCRIPT_DIR}")"
. ${SCRIPT_DIR}/env.sh
. ${SCRIPT_DIR}/build.sh

echo "Please run this now: ngrok http -subdomain=jira-projects-on-tv 8080"
docker-compose -f ${PARENT_DIR}/docker-compose.yml up app
echo "Removing the image..."
docker rmi --force ${DOCKER_IMAGE}:${DOCKER_TAG}
echo "Cleaning containers..."
docker rm $(docker ps -qa)
