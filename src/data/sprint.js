import _ from 'lodash';
import moment from 'moment';

import * as JiraRequest from '../api/jira';
import { getColorForStatus } from './status';
import { getProgress, issueToTicketUpdate } from './common';
import { createActivtyGroups } from './activityGroups';
import { createTeamMembers } from './teamMembers';

const getAllSprintsEpicsStatusAndIssues = async(boardId) => {
  const allSprints = await JiraRequest.getAllSprints(boardId);

  const sprint = _(allSprints.values)
    .filter((sprint) => sprint.state === 'active')
    .first();

  const { issues } = await JiraRequest.getAllIssuesForSprint(boardId, sprint.id);

  const epics = _(issues)
    .filter('fields.epic')
    .map('fields.epic')
    .uniqBy('key')
    .value();

  const users = _(issues)
    .map('fields.assignee')
    .compact()
    .uniqBy('key')
    .value();

  const statusList = _(issues)
    .map('fields.status')
    .uniqBy('id')
    .map((status) => ({
      id: status.id,
      name: status.name,
      color: getColorForStatus(status)
    }))
    .value();

  return { sprint, epics, issues, users, statusList };
};

const buildHeaderProps = function({ sprint, epics, issues, users, statusList }) {
  const overallProgressData = getProgress(issues, statusList, sprint.name);
  const progressByStatusId = _.keyBy(overallProgressData, 'id');

  return {
    sprintName: sprint.name,
    sprintEndDate: sprint.endDate,
    teamMembers: users.map((user) => ({
      name: user.displayName,
      avatarSrc: user.avatarUrls['48x48'],
    })),
    epicCount: _.size(epics),
    issueCount: _.size(issues),
    legendItems: statusList
      .filter((status) => {
        const statusItem = progressByStatusId[status.id];
        return statusItem && statusItem.count > 0;
      })
      .map((status) => ({
        label: status.name,
        color: status.color,
      })),
    overallProgressData,
  };
};

const buildMainContentProps = ({ epics, issues, statusList }) => {
  const issuesByEpic = _.groupBy(issues, 'fields.epic.id');

  if (issuesByEpic[undefined].length > 0) {
    epics.push({
      name: 'Issues not in an Epic',
    });
  }

  const transformedEpics = _(epics)
    .map((epic) => {
      const issues = issuesByEpic[epic.id];

      const users = _(issues)
        .map('fields.assignee')
        .compact()
        .uniqBy('key')
        .value();

      const lastUpdatedIssue = _(issues)
        .filter('fields.updated')
        .sortBy('fields.updated')
        .last();

      const hasBlocker = _(issues)
        .some((issue) => {
          const { status } = issue.fields;
          return [
            'Has Blocker',
            'Blocked',
          ].some(_.partial(_.eq, status.name));
        });

      const name = epic.name || epic.summary;

      return {
        name,
        id: epic.key,
        hasBlocker,
        issueCount: _.size(issues),
        members: users.map((user) => ({
          name: user.displayName,
          avatarSrc: user.avatarUrls['48x48'],
        })),
        progressData: getProgress(issues, statusList, name),
        lastUpdated: lastUpdatedIssue ? {
          timestamp: moment(lastUpdatedIssue.fields.updated).unix(),
          ticketData: issueToTicketUpdate(lastUpdatedIssue),
        } : null
      };
    })
    .orderBy('lastUpdated.timestamp', 'desc')
    .value();

  return {
    epics: transformedEpics,
  };
};

const buildSidebarProps = ({ issues, users }) => {
  return {
    activityGroups: createActivtyGroups(issues),
    members: createTeamMembers({ issues, users }),
  };
};

export const getData = async(boardId) => {
  const { sprint, epics, issues, users, statusList } = await getAllSprintsEpicsStatusAndIssues(boardId);

  const data = {
    sprint,
    epics,
    issues,
    statusList,
    users,
  };

  const result = {
    headerProps: buildHeaderProps(data),
    mainContentProps: buildMainContentProps(data),
    sidebarProps: buildSidebarProps(data),
  };

  return result;
};
