import _ from 'lodash';
import moment from 'moment';

import * as JiraRequest from '../api/jira';
import * as ServiceRequest from '../api/service';
import { getColorForStatus } from './status';
import { getProgress, issueToTicketUpdate } from './common';
import { createActivtyGroups } from './activityGroups';
import { createTeamMembers } from './teamMembers';

const getAllEpicsStatusAndIssues = async(boardId, getAllEpicsFunc, getAllIssuesForEpicFunc, token) => {
  const allEpics = await getAllEpicsFunc(boardId, token);

  const epics = _(allEpics.values)
    .filter((epic) => !epic.done)
    .value();

  const epicChildren = await Promise.all(epics.map(({ id }) => getAllIssuesForEpicFunc(id, token)));

  const issues = _(epicChildren)
    .map((child) => {
      return _(child.issues)
        .map((issue) => {
          const transformedSubtasks = _(issue.fields.subtasks)
            .map((subtask) => {
              subtask.fields.epic = issue.fields.epic;
              return subtask;
            })
            .value();

          return [issue, transformedSubtasks];
        })
        .value();
    })
    .flattenDeep()
    .uniqBy('key')
    .value();

  const users = _(issues)
    .map('fields.assignee')
    .compact()
    .uniqBy('key')
    .value();

  const statusList = _(issues)
    .map('fields.status')
    .uniqBy('id')
    .map((status) => ({
      id: status.id,
      name: status.name,
      color: getColorForStatus(status)
    }))
    .value();

  return { epics, issues, users, statusList };
};

const buildHeaderProps = function({ projectDetails, epics, issues, users, statusList }) {
  const overallProgressData = getProgress(issues, statusList, projectDetails.name);
  const progressByStatusId = _.keyBy(overallProgressData, 'id');

  return {
    projectName: projectDetails.name,
    projectAvatarSrc: projectDetails.avatarUrls['48x48'],
    teamMembers: users.map((user) => ({
      name: user.displayName,
      avatarSrc: user.avatarUrls['48x48'],
    })),
    epicCount: _.size(epics),
    issueCount: _.size(issues),
    legendItems: statusList
      .filter((status) => {
        const statusItem = progressByStatusId[status.id];
        return statusItem && statusItem.count > 0;
      })
      .map((status) => ({
        label: status.name,
        color: status.color,
      })),
    overallProgressData,
  };
};

const buildMainContentProps = ({ epics, issues, statusList }) => {
  const issuesByEpic = _.groupBy(issues, 'fields.epic.id');

  const transformedEpics = _(epics)
    .map((epic) => {
      const issues = issuesByEpic[epic.id];
      const newIssues = _.filter(issues, (issue) => {
        if (!issue.fields.created) {
          return false;
        }
        return moment(issue.fields.created).isAfter(moment().subtract(24, 'hours'));
      });

      const users = _(issues)
        .map('fields.assignee')
        .compact()
        .uniqBy('key')
        .value();

      const lastUpdatedIssue = _(issues)
        .filter('fields.updated')
        .sortBy('fields.updated')
        .last();

      const hasBlocker = _(issues)
        .some((issue) => {
          const { status } = issue.fields;
          return [
            'Has Blocker',
            'Blocked',
          ].some(_.partial(_.eq, status.name));
        });

      const name = epic.name || epic.summary;

      return {
        name,
        id: epic.key,
        hasBlocker,
        newIssueCount: _.size(newIssues),
        issueCount: _.size(issues),
        members: users.map((user) => ({
          name: user.displayName,
          avatarSrc: user.avatarUrls['48x48'],
        })),
        progressData: getProgress(issues, statusList, name),
        lastUpdated: lastUpdatedIssue ? {
          timestamp: moment(lastUpdatedIssue.fields.updated).unix(),
          ticketData: issueToTicketUpdate(lastUpdatedIssue),
        } : null
      };
    })
    .filter(({ lastUpdated }) => {
      if (!lastUpdated) {
        return false;
      }

      const fortnightBefore = moment().subtract(2, 'week');
      return moment.unix(lastUpdated.timestamp).isAfter(fortnightBefore);
    })
    .orderBy('lastUpdated.timestamp', 'desc')
    .value();

  return {
    epics: transformedEpics,
  };
};

const buildSidebarProps = ({ issues, users }) => {
  return {
    activityGroups: createActivtyGroups(issues),
    members: createTeamMembers({ issues, users }),
  };
};

export const getData = async(boardId, projectId) => {
  const [projectDetails, { epics, issues, users, statusList }] = await Promise.all([
    JiraRequest.getProjectDetails(projectId),
    getAllEpicsStatusAndIssues(boardId, JiraRequest.getAllEpics, JiraRequest.getAllIssuesForEpic),
  ]);

  const data = {
    projectDetails,
    epics,
    issues,
    statusList,
    users,
  };

  console.log(JSON.stringify(data));

  const result = {
    headerProps: buildHeaderProps(data),
    mainContentProps: buildMainContentProps(data),
    sidebarProps: buildSidebarProps(data),
  };

  return result;
};

export const getCastData = async(boardId, token) => {

  // const result = {
  //   'headerProps': {
  //     'projectName': 'ShipITXLII',
  //     'projectAvatarSrc': 'https://bc-ac101-mp.atlassian.net/secure/projectavatar?pid=10001&avatarId=10400',
  //     'teamMembers': [],
  //     'epicCount': 1,
  //     'issueCount': 1,
  //     'legendItems': [{ 'label': 'To Do', 'color': '#6554C0' }],
  //     'overallProgressData': [{
  //       'id': '10000',
  //       'collectionName': 'ShipITXLII',
  //       'count': 1,
  //       'color': '#6554C0',
  //       'name': 'To Do',
  //       'ticketsUpdated': [{
  //         'assignee': null,
  //         'ticket': { 'id': 'SPTXLII-25', 'name': 'Make Chromecast button' },
  //         'updatedStatus': { 'label': 'To Do', 'color': '#6554C0' }
  //       }]
  //     }]
  //   },
  //   'mainContentProps': {
  //     'epics': [{
  //       'name': 'Chromecast',
  //       'id': 'SPTXLII-24',
  //       'hasBlocker': false,
  //       'newIssueCount': 1,
  //       'issueCount': 1,
  //       'members': [],
  //       'progressData': [{
  //         'id': '10000',
  //         'collectionName': 'Chromecast',
  //         'count': 1,
  //         'color': '#6554C0',
  //         'name': 'To Do',
  //         'ticketsUpdated': [{
  //           'assignee': null,
  //           'ticket': { 'id': 'SPTXLII-25', 'name': 'Make Chromecast button' },
  //           'updatedStatus': { 'label': 'To Do', 'color': '#6554C0' }
  //         }]
  //       }],
  //       'lastUpdated': {
  //         'timestamp': 1525924055,
  //         'ticketData': {
  //           'assignee': null,
  //           'ticket': { 'id': 'SPTXLII-25', 'name': 'Make Chromecast button' },
  //           'updatedStatus': { 'label': 'To Do', 'color': '#6554C0' }
  //         }
  //       }
  //     }]
  //   },
  //   'sidebarProps': {
  //     'activityGroups': [{
  //       'label': 'Last 3 hours',
  //       'ticketsUpdated': [{
  //         'assignee': null,
  //         'ticket': { 'id': 'SPTXLII-25', 'name': 'Make Chromecast button' },
  //         'updatedStatus': { 'label': 'To Do', 'color': '#6554C0' }
  //       }],
  //       'ticketsToShowInFull': 1
  //     }], 'members': []
  //   }
  // };


  const boardResponse = await fetch(`/board-details?token=${token}&boardId=${boardId}`);
  const boardData = await boardResponse.json();
  const projectId = boardData.location.projectId;

  const [projectDetails, { epics, issues, users, statusList }] = await Promise.all([
    ServiceRequest.getProjectDetails(projectId, token),
    getAllEpicsStatusAndIssues(boardId, ServiceRequest.getAllEpics, ServiceRequest.getAllIssuesForEpic, token),
  ]);

  const data = {
    projectDetails,
    epics,
    issues,
    statusList,
    users,
  };

  console.log(JSON.stringify(data));

  const result = {
    headerProps: buildHeaderProps(data),
    mainContentProps: buildMainContentProps(data),
    sidebarProps: buildSidebarProps(data),
  };

  return result;
};
