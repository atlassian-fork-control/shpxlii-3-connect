import _ from 'lodash';

import { issueToTicketUpdate } from './common';

export const isIssueInProgress = (issue) => {
  const { status } = issue.fields;
  const { key } = status.statusCategory;
  return !_.some([
    'new',
    'done',
  ], _.partial(_.eq, key));
};

export const transformUser = (user, activeIssuesByMember) => {
  const active = activeIssuesByMember[user.key] || [];

  return {
    name: user.displayName,
    avatarSrc: user.avatarUrls['48x48'],
    activeCount: active.length,
    ticketsUpdated: _.map(active, issueToTicketUpdate),
  };
};

export const createTeamMembers = ({ issues, users }) => {
  const activeIssuesByMember = _(issues)
    .filter(isIssueInProgress)
    .groupBy('fields.assignee.key')
    .value();

  return _(users)
    .map((user) => transformUser(user, activeIssuesByMember))
    .value();
};
