import _ from 'lodash';
import moment from 'moment';

import { issueToTicketUpdate } from './common';
import { issueStatusComparator } from './comparators';

export const MAX_ACTIVITY_ITEMS = 9;

export const MAX_ACTIVITY_GROUPS = 3;

export const ACTIVITY_GROUP_ORDER = [
  'Last 30 minutes',
  'Last 3 hours',
  'Last day',
  'Last 3 days',
  'Last week',
  'Last month',
  'Last 3 months',
  'Longer than 3 months',
];

export const issueToGroupName = (issue) => {
  const time = moment(issue.fields.updated);
  const now = moment();
  if (time.isAfter(now.subtract(30, 'minute'))) {
    return 'Last 30 minutes';
  }

  if (time.isAfter(now.subtract(3, 'hour'))) {
    return 'Last 3 hours';
  }

  if (time.isAfter(now.subtract(24, 'hour'))) {
    return 'Last day';
  }

  if (time.isAfter(now.subtract(24 * 3, 'hour'))) {
    return 'Last 3 days';
  }

  if (time.isAfter(now.subtract(1, 'week'))) {
    return 'Last week';
  }

  if (time.isAfter(now.subtract(1, 'month'))) {
    return 'Last month';
  }

  if (time.isAfter(now.subtract(3, 'month'))) {
    return 'Last 3 months';
  }

  return 'Longer than 3 months';
};

export const createActivtyGroups = (issues) => {
  const groupedIssues = _(issues)
    .filter('fields.updated')
    .groupBy(issueToGroupName)
    .value();

  return _(ACTIVITY_GROUP_ORDER)
    .reduce(({ list, remaining }, key) => {
      const group = groupedIssues[key];
      if (!group || list.length >= MAX_ACTIVITY_GROUPS || remaining < 1) {
        return { list, remaining };
      }

      const ticketsUpdated = groupedIssues[key].map(issueToTicketUpdate);
      ticketsUpdated.sort(issueStatusComparator);

      const ticketsToShowInFull = ticketsUpdated.length > remaining ? remaining : ticketsUpdated.length;

      list.push({
        label: key,
        ticketsUpdated,
        ticketsToShowInFull,
      });

      return {
        list,
        remaining: remaining - ticketsToShowInFull,
      };
    }, {
      list: [],
      remaining: MAX_ACTIVITY_ITEMS,
    }).list;
};
