const sprintIssues = { // eslint-disable-line no-unused-vars
  'expand': 'schema,names',
  'startAt': 0,
  'maxResults': 50,
  'total': 61,
  'issues': [{
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114241',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114241',
    'key': 'MEGATRON-571',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 110105,
        'key': 'MEGATRON-277',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/110105',
        'name': 'Software Team Pages',
        'summary': 'Software Team Pages',
        'color': { 'key': 'color_4' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/1',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/highest.svg',
        'name': 'Highest',
        'id': '1'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [{
        'id': '54923',
        'self': 'https://growth.jira-dev.com/rest/api/2/issueLink/54923',
        'type': {
          'id': '10202',
          'name': 'Dependent',
          'inward': 'is depended on by',
          'outward': 'depends on',
          'self': 'https://growth.jira-dev.com/rest/api/2/issueLinkType/10202'
        },
        'inwardIssue': {
          'id': '114721',
          'key': 'MEGATRON-690',
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114721',
          'fields': {
            'summary': 'Roll Pages and Backlog View out to 80%',
            'status': {
              'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
              'description': '',
              'iconUrl': 'https://growth.jira-dev.com/',
              'name': 'Done',
              'id': '10001',
              'statusCategory': {
                'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
                'id': 3,
                'key': 'done',
                'colorName': 'green',
                'name': 'Done'
              }
            },
            'priority': {
              'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
              'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
              'name': 'Medium',
              'id': '3'
            },
            'issuetype': {
              'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
              'id': '10002',
              'description': 'A task that needs to be done.',
              'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
              'name': 'Task',
              'subtask': false,
              'avatarId': 10318
            }
          }
        }
      }],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
        'name': 'ahammond',
        'key': 'ahammond',
        'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
        'emailAddress': 'ahammond@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Hammond',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-571/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T13:05:04.808+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-571/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-01-09T15:19:18.541+1100',
      'customfield_12200': null,
      'customfield_10022': 2.0,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_10016': '0|hzzzfw:i21x012if1',
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_13800': null,
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=2}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":2,"lastUpdated":"2018-02-19T10:52:31.463+1100","stateCount":2,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"stash":{"count":2,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_10018': 'MEGATRON-277',
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-21T13:05:04.813+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'Server error is always shown.\r\n\r\n!Screen Shot 2018-01-09 at 3.17.18 pm.png|thumbnail! \r\n\r\nUse the new gateway api endpoints in JFE and xflow. Bump versions of xflow everywhere.\r\n\r\nMake sure we no longer exclude Safari.\r\n\r\n',
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [{
        'self': 'https://growth.jira-dev.com/rest/api/2/attachment/72739',
        'id': '72739',
        'filename': 'Screen Shot 2018-01-09 at 3.17.18 pm.png',
        'author': {
          'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
          'name': 'iflores',
          'key': 'iflores',
          'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
          'emailAddress': 'iflores@atlassian.com',
          'avatarUrls': {
            '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
            '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
            '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
            '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
          },
          'displayName': 'Isabelle Flores',
          'active': true,
          'timeZone': 'Australia/Sydney'
        },
        'created': '2018-01-09T15:19:17.244+1100',
        'size': 127893,
        'mimeType': 'image/png',
        'content': 'https://growth.jira-dev.com/secure/attachment/72739/Screen+Shot+2018-01-09+at+3.17.18+pm.png',
        'thumbnail': 'https://growth.jira-dev.com/secure/thumbnail/72739/Screen+Shot+2018-01-09+at+3.17.18+pm.png'
      }],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'CORS errors in Safari',
      'customfield_10000': '2018-01-10T10:59:35.054+1100',
      'customfield_10001': '10117_*:*_1_*:*_2933030367_*|*_3_*:*_1_*:*_81030805_*|*_10000_*:*_1_*:*_266658346_*|*_12200_*:*_1_*:*_245784516_*|*_12400_*:*_1_*:*_84043188_*|*_12600_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114241/comment/179235',
          'id': '179235',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=gburrows',
            'name': 'gburrows',
            'key': 'gburrows',
            'accountId': '655363:b2b676b2-702e-4750-81ff-de691e7a5319',
            'emailAddress': 'gburrows@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'George Burrows',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "[~awakeling] This endpoint is deprecated. While you're in here, please could you change the endpoint to \r\n\r\n/:cloudId/license-information \r\n\r\nas per:\r\n\r\nhttps://growth.jira-dev.com/browse/MEGATRON-565\r\n\r\nThis will ensure you're testing against the correct endpoint.",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=gburrows',
            'name': 'gburrows',
            'key': 'gburrows',
            'accountId': '655363:b2b676b2-702e-4750-81ff-de691e7a5319',
            'emailAddress': 'gburrows@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'George Burrows',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-01-10T10:59:35.054+1100',
          'updated': '2018-01-10T10:59:35.054+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114241/comment/179305',
          'id': '179305',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
            'name': 'lhunt',
            'key': 'lhunt',
            'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
            'emailAddress': 'lhunt@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
            },
            'displayName': 'Lachlan Hunt',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'https://product-fabric.atlassian.net/wiki/spaces/FS/pages/370246377/How+do+we+make+sure+the+user+still+have+a+valid+session+cookie+in+Confluence+and+Jira\r\n\r\nhttps://extranet.atlassian.com/pages/viewpage.action?pageId=3675986606',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
            'name': 'lhunt',
            'key': 'lhunt',
            'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
            'emailAddress': 'lhunt@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
            },
            'displayName': 'Lachlan Hunt',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-01-12T17:15:12.972+1100',
          'updated': '2018-01-12T17:15:12.972+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114241/comment/179306',
          'id': '179306',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
            'name': 'lhunt',
            'key': 'lhunt',
            'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
            'emailAddress': 'lhunt@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
            },
            'displayName': 'Lachlan Hunt',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "I've updated the license-information URL.",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
            'name': 'lhunt',
            'key': 'lhunt',
            'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
            'emailAddress': 'lhunt@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
            },
            'displayName': 'Lachlan Hunt',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-01-12T17:27:01.857+1100',
          'updated': '2018-01-12T17:27:01.857+1100'
        }], 'maxResults': 3, 'total': 3, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114342',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114342',
    'key': 'MEGATRON-637',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 110105,
        'key': 'MEGATRON-277',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/110105',
        'name': 'Software Team Pages',
        'summary': 'Software Team Pages',
        'color': { 'key': 'color_4' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=awakeling',
        'name': 'awakeling',
        'key': 'awakeling',
        'accountId': '655363:bebdffcd-d1ee-4219-98a7-8b484a612112',
        'emailAddress': 'awakeling@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Wakeling',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-637/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
        'id': '10001',
        'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
        'name': 'Story',
        'subtask': false
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-19T16:11:55.981+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-637/watchers',
        'watchCount': 0,
        'isWatching': false
      },
      'created': '2018-01-15T15:58:06.341+1100',
      'customfield_12200': null,
      'customfield_10022': 1.0,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_10016': '0|hzzzfw:i21x012if1i',
      'customfield_11502': null,
      'customfield_11501': '{repository={count=7, dataType=repository}, json={"cachedValue":{"errors":[],"summary":{"repository":{"overall":{"count":7,"lastUpdated":"2018-02-14T17:31:06.000+1100","dataType":"repository"},"byInstanceType":{"stash":{"count":7,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13803': null,
      'customfield_10018': 'MEGATRON-277',
      'customfield_13802': null,
      'updated': '2018-02-19T16:11:55.990+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': null,
      'customfield_13000': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Fix IE CSS bugs (after blank screen issue resolved)',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_1025222391_*|*_10000_*:*_1_*:*_1559391836_*|*_12200_*:*_1_*:*_5720356_*|*_12400_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114605',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114605',
    'key': 'EUE-54',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=awakeling',
        'name': 'awakeling',
        'key': 'awakeling',
        'accountId': '655363:bebdffcd-d1ee-4219-98a7-8b484a612112',
        'emailAddress': 'awakeling@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Wakeling',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/12200',
        'description': 'This status is managed internally by JIRA Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Waiting on PR',
        'id': '12200',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=awakeling',
        'name': 'awakeling',
        'key': 'awakeling',
        'accountId': '655363:bebdffcd-d1ee-4219-98a7-8b484a612112',
        'emailAddress': 'awakeling@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Wakeling',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=awakeling',
        'name': 'awakeling',
        'key': 'awakeling',
        'accountId': '655363:bebdffcd-d1ee-4219-98a7-8b484a612112',
        'emailAddress': 'awakeling@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Wakeling',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-54/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-54/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-31T16:32:39.003+1100',
      'customfield_12200': null,
      'customfield_10022': 1.0,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_10016': '0|hzzzfw:i21x012if2',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_11501': '{}',
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-15T16:07:13.960+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': 'FYI: Creating this task in EUE to raise awareness on the board.\r\n\r\nhttps://bitbucket.org/atlassian/cloud-analytics-js/pull-requests/11',
      'customfield_13000': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Roll out cloud-analytics-js changes to prod',
      'customfield_10000': null,
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114605/comment/179720',
          'id': '179720',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=awakeling',
            'name': 'awakeling',
            'key': 'awakeling',
            'accountId': '655363:bebdffcd-d1ee-4219-98a7-8b484a612112',
            'emailAddress': 'awakeling@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Andrew Wakeling',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'https://bitbucket.org/atlassian/cloud-analytics/pull-requests/81/jfp-979-mitigate-cloud-analytics-js/diff',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=awakeling',
            'name': 'awakeling',
            'key': 'awakeling',
            'accountId': '655363:bebdffcd-d1ee-4219-98a7-8b484a612112',
            'emailAddress': 'awakeling@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Andrew Wakeling',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-15T16:07:06.255+1100',
          'updated': '2018-02-15T16:07:06.255+1100'
        }], 'maxResults': 1, 'total': 1, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114351',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114351',
    'key': 'MEGATRON-640',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
        'name': 'ahammond',
        'key': 'ahammond',
        'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
        'emailAddress': 'ahammond@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Hammond',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=awakeling',
        'name': 'awakeling',
        'key': 'awakeling',
        'accountId': '655363:bebdffcd-d1ee-4219-98a7-8b484a612112',
        'emailAddress': 'awakeling@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Wakeling',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=awakeling',
        'name': 'awakeling',
        'key': 'awakeling',
        'accountId': '655363:bebdffcd-d1ee-4219-98a7-8b484a612112',
        'emailAddress': 'awakeling@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Wakeling',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 161,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/161',
        'state': 'closed',
        'name': 'Megatron - Goon - 13',
        'startDate': '2018-01-15T05:20:31.919Z',
        'endDate': '2018-01-25T05:10:00.000Z',
        'completeDate': '2018-01-30T23:42:58.480Z',
        'originBoardId': 62,
        'goal': 'Feature complete with New lozenge, Meatball menu and Opt-Out, without support for IE/Safari.'
      }, {
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-640/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-23T16:23:12.447+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-640/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-16T16:53:41.185+1100',
      'customfield_12200': null,
      'customfield_10022': 0.5,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06cx2:x',
      'customfield_11502': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@251cab09[id=161,rapidViewId=62,state=CLOSED,name=Megatron - Goon - 13,goal=Feature complete with New lozenge, Meatball menu and Opt-Out, without support for IE/Safari.,startDate=2018-01-15T05:20:31.919Z,endDate=2018-01-25T05:10:00.000Z,completeDate=2018-01-30T23:42:58.480Z,sequence=161]', 'com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{}',
      'customfield_13800': null,
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-23T16:23:12.452+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': 'Setup "Revolver" chrome extension to cycle through appropriate tabs relating to xflow.',
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Setup xflow service dashboard on nearby screen',
      'customfield_10000': null,
      'customfield_10001': '10117_*:*_1_*:*_0_*|*_3_*:*_1_*:*_22509186_*|*_10000_*:*_1_*:*_2916089845',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114372',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114372',
    'key': 'MEGATRON-647',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 110105,
        'key': 'MEGATRON-277',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/110105',
        'name': 'Software Team Pages',
        'summary': 'Software Team Pages',
        'color': { 'key': 'color_4' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
        'name': 'ahammond',
        'key': 'ahammond',
        'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
        'emailAddress': 'ahammond@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Hammond',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lbalan',
        'name': 'lbalan',
        'key': 'lbalan',
        'accountId': '655362:bb6aca28-e148-4b23-a62c-7f865f2ede4c',
        'emailAddress': 'lbalan@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Leandro Balan',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lbalan',
        'name': 'lbalan',
        'key': 'lbalan',
        'accountId': '655362:bb6aca28-e148-4b23-a62c-7f865f2ede4c',
        'emailAddress': 'lbalan@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Leandro Balan',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-647/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-20T10:15:08.737+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-647/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-01-17T14:56:40.819+1100',
      'customfield_12200': null,
      'customfield_10022': 1.0,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06cx2:xi',
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=OPEN, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-28T11:25:43.430+1100","stateCount":1,"state":"OPEN","dataType":"pullrequest","open":true},"byInstanceType":{"stash":{"count":1,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_13803': null,
      'customfield_10018': 'MEGATRON-277',
      'customfield_13802': null,
      'updated': '2018-02-20T10:15:08.745+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': "During this early rollout stage particularly it would be very helpful to provide a direct line to users for feedback on Pages.\r\n\r\nThis does not have to be anything polished/fancy - we should try to think of a low-cost solution if possible. One that comes to mind is using Jira's own issue collector, for example:\r\n\r\n !screenshot-1.png|thumbnail! \r\n !screenshot-2.png|thumbnail! ",
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [{
        'self': 'https://growth.jira-dev.com/rest/api/2/attachment/72774',
        'id': '72774',
        'filename': 'screenshot-1.png',
        'author': {
          'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lbalan',
          'name': 'lbalan',
          'key': 'lbalan',
          'accountId': '655362:bb6aca28-e148-4b23-a62c-7f865f2ede4c',
          'emailAddress': 'lbalan@atlassian.com',
          'avatarUrls': {
            '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
            '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
            '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
            '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
          },
          'displayName': 'Leandro Balan',
          'active': true,
          'timeZone': 'Australia/Sydney'
        },
        'created': '2018-01-17T15:03:15.167+1100',
        'size': 371510,
        'mimeType': 'image/png',
        'content': 'https://growth.jira-dev.com/secure/attachment/72774/screenshot-1.png',
        'thumbnail': 'https://growth.jira-dev.com/secure/thumbnail/72774/screenshot-1.png'
      }, {
        'self': 'https://growth.jira-dev.com/rest/api/2/attachment/72775',
        'id': '72775',
        'filename': 'screenshot-2.png',
        'author': {
          'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lbalan',
          'name': 'lbalan',
          'key': 'lbalan',
          'accountId': '655362:bb6aca28-e148-4b23-a62c-7f865f2ede4c',
          'emailAddress': 'lbalan@atlassian.com',
          'avatarUrls': {
            '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
            '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
            '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
            '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
          },
          'displayName': 'Leandro Balan',
          'active': true,
          'timeZone': 'Australia/Sydney'
        },
        'created': '2018-01-17T15:03:49.285+1100',
        'size': 412403,
        'mimeType': 'image/png',
        'content': 'https://growth.jira-dev.com/secure/attachment/72775/screenshot-2.png',
        'thumbnail': 'https://growth.jira-dev.com/secure/thumbnail/72775/screenshot-2.png'
      }],
      'flagged': false,
      'summary': 'Add feedback functionality to Pages to collect user feedback',
      'customfield_10000': '2018-02-07T16:55:30.036+1100',
      'customfield_10001': '10117_*:*_1_*:*_599579302_*|*_3_*:*_3_*:*_108087995_*|*_10000_*:*_2_*:*_1807228286_*|*_12200_*:*_1_*:*_321769813_*|*_12400_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114372/comment/179687',
          'id': '179687',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lbalan',
            'name': 'lbalan',
            'key': 'lbalan',
            'accountId': '655362:bb6aca28-e148-4b23-a62c-7f865f2ede4c',
            'emailAddress': 'lbalan@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Leandro Balan',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "I've just logged an atlasdesk issue to create a project on JAC & give us access to configure an issue collector on it:\r\nhttps://extranet.atlassian.com/jira/servicedesk/customer/portal/2/ADM-159967\r\n\r\n:fingerscrossed:",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lbalan',
            'name': 'lbalan',
            'key': 'lbalan',
            'accountId': '655362:bb6aca28-e148-4b23-a62c-7f865f2ede4c',
            'emailAddress': 'lbalan@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/447ffb2110143f9a852f3d16aa1ad2af?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F447ffb2110143f9a852f3d16aa1ad2af%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Leandro Balan',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-07T16:48:32.100+1100',
          'updated': '2018-02-07T16:48:32.100+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114372/comment/179688',
          'id': '179688',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
            'name': 'ahammond',
            'key': 'ahammond',
            'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
            'emailAddress': 'ahammond@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
            },
            'displayName': 'Andrew Hammond',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': '\\o/ ty [~lbalan] ',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
            'name': 'ahammond',
            'key': 'ahammond',
            'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
            'emailAddress': 'ahammond@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
            },
            'displayName': 'Andrew Hammond',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-07T16:55:30.036+1100',
          'updated': '2018-02-07T16:55:30.036+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114372/comment/179711',
          'id': '179711',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
            'name': 'ahammond',
            'key': 'ahammond',
            'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
            'emailAddress': 'ahammond@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
            },
            'displayName': 'Andrew Hammond',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "Atlasdesk came back and I've set up an issue collector for JAC in their Feedback project - https://jira.atlassian.com/secure/ViewCollector!default.jspa?projectKey=FEEDBACK&collectorId=3056b2af\r\n\r\nJust need to check with [~lbalan] that this is an okay solution - especially as I have him down as the reporter for every issue that would be created through it. \r\n\r\nI also need to check with [~htapia] around the design for this feedback button and how it looks on the page. \r\n",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
            'name': 'ahammond',
            'key': 'ahammond',
            'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
            'emailAddress': 'ahammond@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
            },
            'displayName': 'Andrew Hammond',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-13T16:14:16.186+1100',
          'updated': '2018-02-13T16:14:16.186+1100'
        }], 'maxResults': 3, 'total': 3, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114114',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114114',
    'key': 'MEGATRON-493',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': '2018-02-15T11:39:12.567+1100',
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 110105,
        'key': 'MEGATRON-277',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/110105',
        'name': 'Software Team Pages',
        'summary': 'Software Team Pages',
        'color': { 'key': 'color_4' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10117',
        'description': 'This status is managed internally by JIRA Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Blocked',
        'id': '10117',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [{
        'id': '114683',
        'key': 'MEGATRON-676',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114683',
        'fields': {
          'summary': 'Change usage of /_tenant_info in xflow to new endpoint after it becomes available',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114684',
        'key': 'MEGATRON-677',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114684',
        'fields': {
          'summary': 'Replace endpoint to check whether the user has "activate product" permissions',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10117',
            'description': 'This status is managed internally by JIRA Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Blocked',
            'id': '10117',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114685',
        'key': 'MEGATRON-678',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114685',
        'fields': {
          'summary': 'Replace endpoint to check user context',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10117',
            'description': 'This status is managed internally by JIRA Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Blocked',
            'id': '10117',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-493/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-493/watchers',
        'watchCount': 2,
        'isWatching': true
      },
      'created': '2017-12-27T16:33:23.302+1100',
      'customfield_12200': null,
      'customfield_10022': 0.5,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06e8j:o',
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-13T14:57:51.000+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"bitbucket":{"count":1,"name":"Bitbucket Cloud"}}}}},"isStale":true}}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_13803': null,
      'customfield_10018': 'MEGATRON-277',
      'customfield_13802': null,
      'updated': '2018-02-23T11:28:44.529+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': "Once Team Rocket completes the necessary changes for xflow component to query for the current user's global context + permissions to activate product / add users, then xflow component will need to update the endpoint it hits so that we can turn on the usage of it in GAB again in a Stride only account state.\r\n\r\nDepends on issue: https://trello.com/c/9vFkxleQ/218-support-for-productionization-of-product-activation-experiment\r\nOriginal issue: https://growth.jira-dev.com/browse/MEGATRON-476\r\n\r\ncc: [~gburrows] [~mpuckeridge]",
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Update atlaskit/xflow component endpoints to product agnostic ones',
      'customfield_10000': '2018-02-07T11:13:13.818+1100',
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114114/comment/179168',
          'id': '179168',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'https://extranet.atlassian.com/display/ISS/CSE+-+DACI+Expose+CloudId+publicly+from+the+Atlassian+Proxy',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-01-05T16:49:13.662+1100',
          'updated': '2018-01-05T16:49:13.662+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114114/comment/179686',
          'id': '179686',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
            'name': 'lhunt',
            'key': 'lhunt',
            'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
            'emailAddress': 'lhunt@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
            },
            'displayName': 'Lachlan Hunt',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Blocked until the tenant info endpoint is moved to its new location (probably something under /gateway/)',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
            'name': 'lhunt',
            'key': 'lhunt',
            'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
            'emailAddress': 'lhunt@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
            },
            'displayName': 'Lachlan Hunt',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-07T11:13:13.818+1100',
          'updated': '2018-02-07T11:13:13.818+1100'
        }], 'maxResults': 2, 'total': 2, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114683',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114683',
    'key': 'MEGATRON-676',
    'fields': {
      'parent': {
        'id': '114114',
        'key': 'MEGATRON-493',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114114',
        'fields': {
          'summary': 'Update atlaskit/xflow component endpoints to product agnostic ones',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10117',
            'description': 'This status is managed internally by JIRA Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Blocked',
            'id': '10117',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-676/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-23T16:47:24.235+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-676/watchers',
        'watchCount': 0,
        'isWatching': false
      },
      'created': '2018-02-07T11:19:30.382+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06fhj:',
      'customfield_11502': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_11501': '{}',
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-23T16:47:24.245+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': "The /_tenant_info endpoint will move to /_edge/tenant_info\r\n\r\nUpdate this in atlaskit/xflow component when it's available.",
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Change usage of /_tenant_info in xflow to new endpoint after it becomes available',
      'customfield_10000': null,
      'customfield_10001': '10000_*:*_1_*:*_639786740_*|*_12200_*:*_1_*:*_162369848_*|*_12400_*:*_1_*:*_492242265_*|*_10001_*:*_2_*:*_258044_*|*_12600_*:*_1_*:*_107416973',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114684',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114684',
    'key': 'MEGATRON-677',
    'fields': {
      'parent': {
        'id': '114114',
        'key': 'MEGATRON-493',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114114',
        'fields': {
          'summary': 'Update atlaskit/xflow component endpoints to product agnostic ones',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10117',
            'description': 'This status is managed internally by JIRA Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Blocked',
            'id': '10117',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': null,
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10117',
        'description': 'This status is managed internally by JIRA Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Blocked',
        'id': '10117',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-677/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-677/watchers',
        'watchCount': 0,
        'isWatching': false
      },
      'created': '2018-02-07T11:59:46.758+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06fhr:',
      'customfield_13801': null,
      'customfield_13800': null,
      'customfield_11501': '{}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-16T14:11:02.966+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Replace endpoint to check whether the user has "activate product" permissions',
      'customfield_10000': null,
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114685',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114685',
    'key': 'MEGATRON-678',
    'fields': {
      'parent': {
        'id': '114114',
        'key': 'MEGATRON-493',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114114',
        'fields': {
          'summary': 'Update atlaskit/xflow component endpoints to product agnostic ones',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10117',
            'description': 'This status is managed internally by JIRA Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Blocked',
            'id': '10117',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': null,
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10117',
        'description': 'This status is managed internally by JIRA Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Blocked',
        'id': '10117',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-678/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-678/watchers',
        'watchCount': 0,
        'isWatching': false
      },
      'created': '2018-02-07T12:00:36.452+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06fhz:',
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_13800': null,
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-19T16:41:35.000+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"bitbucket":{"count":1,"name":"Bitbucket Cloud"}}}}},"isStale":true}}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-16T14:11:04.086+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'The /me endpoint needs to be made available to call from anywhere.',
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Replace endpoint to check user context',
      'customfield_10000': null,
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114685/comment/179725',
          'id': '179725',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
            'name': 'lhunt',
            'key': 'lhunt',
            'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
            'emailAddress': 'lhunt@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
            },
            'displayName': 'Lachlan Hunt',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'https://extranet.atlassian.com/display/PGT/MEGATRON-476+-+XFlow+needs+replacement+endpoints',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
            'name': 'lhunt',
            'key': 'lhunt',
            'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
            'emailAddress': 'lhunt@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
            },
            'displayName': 'Lachlan Hunt',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-16T13:06:22.817+1100',
          'updated': '2018-02-16T13:06:22.817+1100'
        }], 'maxResults': 1, 'total': 1, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114675',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114675',
    'key': 'MEGATRON-675',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 110105,
        'key': 'MEGATRON-277',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/110105',
        'name': 'Software Team Pages',
        'summary': 'Software Team Pages',
        'color': { 'key': 'color_4' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
        'name': 'ahammond',
        'key': 'ahammond',
        'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
        'emailAddress': 'ahammond@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Hammond',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-675/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10004',
        'id': '10004',
        'description': 'A problem which impairs or prevents the functions of the product.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10303&avatarType=issuetype',
        'name': 'Bug',
        'subtask': false,
        'avatarId': 10303
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-16T10:31:30.360+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-675/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-02-05T14:43:16.885+1100',
      'customfield_12200': null,
      'customfield_10022': 0.0,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06e8j:p',
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_13800': null,
      'customfield_11501': '{repository={count=19, dataType=repository}, json={"cachedValue":{"errors":[],"summary":{"repository":{"overall":{"count":19,"lastUpdated":"2018-02-16T15:36:36.000+1100","dataType":"repository"},"byInstanceType":{"stash":{"count":19,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13803': null,
      'customfield_10018': 'MEGATRON-277',
      'customfield_13802': null,
      'updated': '2018-02-16T10:31:30.367+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'Problem outlined by [~hchow] on https://extranet.atlassian.com/display/PGT/Space+creation+and+linking+API\r\n\r\n*Solution*\r\nAdd "x-atlassian-mau-ignore" header to confluence requests in project-pages requests to Confluence.\r\n\r\nEyeball events\r\nThe MAU event filter in Confluence suggests that there is a particular header you can use to skip mau events.\r\n\r\nstatic final String IGNORE_MAU_HEADER = "x-atlassian-mau-ignore";\r\n(from https://stash.atlassian.com/projects/CONFCLOUD/repos/confluence/browse/confluence-core/confluence/src/java/com/atlassian/confluence/web/filter/MauEventFilter.java#53)\r\n',
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Confluence requests from Software Team Pages may be firing MAU events',
      'customfield_10000': '2018-02-07T17:18:56.456+1100',
      'customfield_10001': '3_*:*_2_*:*_22766040_*|*_10000_*:*_2_*:*_159505796_*|*_12200_*:*_1_*:*_60281214_*|*_12400_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114675/comment/179642',
          'id': '179642',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Additional writeup by [~awakeling]\r\nhttps://extranet.atlassian.com/pages/viewpage.action?pageId=3675990492',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-05T14:51:31.122+1100',
          'updated': '2018-02-05T14:51:31.122+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114675/comment/179689',
          'id': '179689',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
            'name': 'ahammond',
            'key': 'ahammond',
            'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
            'emailAddress': 'ahammond@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
            },
            'displayName': 'Andrew Hammond',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Added this to all endpoints calling out to Confluence from Pages itself.\r\n\r\nThis needs to be added to the calls to Confluence from the Page Tree component as well if being called from outside of Confluence (eg. here in Jira from pages). Had a chat with [~grenganathan] who is going to look into it. ',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
            'name': 'ahammond',
            'key': 'ahammond',
            'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
            'emailAddress': 'ahammond@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
            },
            'displayName': 'Andrew Hammond',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-07T17:18:56.456+1100',
          'updated': '2018-02-07T17:18:56.456+1100'
        }], 'maxResults': 2, 'total': 2, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114721',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114721',
    'key': 'MEGATRON-690',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 110105,
        'key': 'MEGATRON-277',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/110105',
        'name': 'Software Team Pages',
        'summary': 'Software Team Pages',
        'color': { 'key': 'color_4' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [{
        'id': '54923',
        'self': 'https://growth.jira-dev.com/rest/api/2/issueLink/54923',
        'type': {
          'id': '10202',
          'name': 'Dependent',
          'inward': 'is depended on by',
          'outward': 'depends on',
          'self': 'https://growth.jira-dev.com/rest/api/2/issueLinkType/10202'
        },
        'outwardIssue': {
          'id': '114241',
          'key': 'MEGATRON-571',
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114241',
          'fields': {
            'summary': 'CORS errors in Safari',
            'status': {
              'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
              'description': '',
              'iconUrl': 'https://growth.jira-dev.com/',
              'name': 'Done',
              'id': '10001',
              'statusCategory': {
                'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
                'id': 3,
                'key': 'done',
                'colorName': 'green',
                'name': 'Done'
              }
            },
            'priority': {
              'self': 'https://growth.jira-dev.com/rest/api/2/priority/1',
              'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/highest.svg',
              'name': 'Highest',
              'id': '1'
            },
            'issuetype': {
              'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
              'id': '10002',
              'description': 'A task that needs to be done.',
              'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
              'name': 'Task',
              'subtask': false,
              'avatarId': 10318
            }
          }
        }
      }],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-690/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-23T16:57:12.190+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-690/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-15T11:32:11.847+1100',
      'customfield_12200': null,
      'customfield_10022': 0.5,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06e8j:q9',
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=2}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":2,"lastUpdated":"2018-02-22T14:17:32.000+1100","stateCount":2,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"bitbucket":{"count":2,"name":"Bitbucket Cloud"}}}}},"isStale":true}}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_10018': 'MEGATRON-277',
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-23T16:57:12.194+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'Cohort Picker settings:\r\n\r\nFor all Megatron touch points:\r\nmk1: 0-50\r\nmegatron-variation: 50-80\r\nmegatron-control: 80-100',
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Roll Pages and Backlog View out to 80%',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_1197086_*|*_10000_*:*_1_*:*_603246449_*|*_12200_*:*_1_*:*_85366071_*|*_12600_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114648',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114648',
    'key': 'MEGATRON-673',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 108703,
        'key': 'MEGATRON-242',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/108703',
        'name': 'Megatron Tech Debt',
        'summary': 'Bug fixes left over in Megatron work',
        'color': { 'key': 'color_9' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-673/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-23T16:28:33.244+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-673/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-02T13:08:51.223+1100',
      'customfield_12200': null,
      'customfield_10022': 1.0,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06e8j:qi',
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=2}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":2,"lastUpdated":"2018-02-15T15:23:53.065+1100","stateCount":2,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"stash":{"count":2,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10018': 'MEGATRON-242',
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-23T16:28:33.249+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': "See https://stash.atlassian.com/projects/JIRACLOUD/repos/jira-frontend/pull-requests/5871/overview?commentId=1025566 for context.\r\n\r\n!Screen Shot 2018-02-02 at 11.34.45 am.png|thumbnail! \r\n\r\nSee how it's done for the Components page: https://stash.atlassian.com/projects/JIRACLOUD/repos/jira/pull-requests/8523/diff#jira-components/jira-plugins/jira-projects/jira-projects-plugin/src/main/resources/page/project/components/templates.soy",
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [{
        'self': 'https://growth.jira-dev.com/rest/api/2/attachment/72813',
        'id': '72813',
        'filename': 'Screen Shot 2018-02-02 at 11.34.45 am.png',
        'author': {
          'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
          'name': 'iflores',
          'key': 'iflores',
          'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
          'emailAddress': 'iflores@atlassian.com',
          'avatarUrls': {
            '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
            '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
            '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
            '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
          },
          'displayName': 'Isabelle Flores',
          'active': true,
          'timeZone': 'Australia/Sydney'
        },
        'created': '2018-02-02T13:06:55.873+1100',
        'size': 86453,
        'mimeType': 'image/png',
        'content': 'https://growth.jira-dev.com/secure/attachment/72813/Screen+Shot+2018-02-02+at+11.34.45+am.png',
        'thumbnail': 'https://growth.jira-dev.com/secure/thumbnail/72813/Screen+Shot+2018-02-02+at+11.34.45+am.png'
      }],
      'flagged': false,
      'summary': 'Remove extra AUI template wrapping around page',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_92992757_*|*_10000_*:*_1_*:*_253963283_*|*_12200_*:*_1_*:*_77399609_*|*_12600_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114722',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114722',
    'key': 'EUE-67',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 108703,
        'key': 'MEGATRON-242',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/108703',
        'name': 'Megatron Tech Debt',
        'summary': 'Bug fixes left over in Megatron work',
        'color': { 'key': 'color_9' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
        'name': 'ahammond',
        'key': 'ahammond',
        'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
        'emailAddress': 'ahammond@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Hammond',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-67/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-20T16:51:37.631+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-67/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-15T11:34:09.344+1100',
      'customfield_12200': null,
      'customfield_10022': 0.5,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06e8j:qr',
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_13800': null,
      'customfield_11501': '{}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_10018': 'MEGATRON-242',
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-20T16:51:37.638+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'https://extranet.atlassian.com/pages/viewpage.action?pageId=3754789485',
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Investigate triggered events firing in multiple cohorts for the same instance',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_98101209_*|*_10000_*:*_1_*:*_352947090_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114227',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114227',
    'key': 'MEGATRON-564',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 108703,
        'key': 'MEGATRON-242',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/108703',
        'name': 'Megatron Tech Debt',
        'summary': 'Bug fixes left over in Megatron work',
        'color': { 'key': 'color_9' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [{
        'id': '54394',
        'self': 'https://growth.jira-dev.com/rest/api/2/issueLink/54394',
        'type': {
          'id': '10205',
          'name': 'Blocked',
          'inward': 'blocked by',
          'outward': 'blocks',
          'self': 'https://growth.jira-dev.com/rest/api/2/issueLinkType/10205'
        },
        'inwardIssue': {
          'id': '114229',
          'key': 'MEGATRON-565',
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114229',
          'fields': {
            'summary': 'Remove deprecated endpoints from xflow service after clients/consumers have been deployed',
            'status': {
              'self': 'https://growth.jira-dev.com/rest/api/2/status/10000',
              'description': '',
              'iconUrl': 'https://growth.jira-dev.com/',
              'name': 'To Do',
              'id': '10000',
              'statusCategory': {
                'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/2',
                'id': 2,
                'key': 'new',
                'colorName': 'blue-gray',
                'name': 'To Do'
              }
            },
            'priority': {
              'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
              'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
              'name': 'Medium',
              'id': '3'
            },
            'issuetype': {
              'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
              'id': '10001',
              'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
              'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
              'name': 'Story',
              'subtask': false
            }
          }
        }
      }],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=gburrows',
        'name': 'gburrows',
        'key': 'gburrows',
        'accountId': '655363:b2b676b2-702e-4750-81ff-de691e7a5319',
        'emailAddress': 'gburrows@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'George Burrows',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=gburrows',
        'name': 'gburrows',
        'key': 'gburrows',
        'accountId': '655363:b2b676b2-702e-4750-81ff-de691e7a5319',
        'emailAddress': 'gburrows@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'George Burrows',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-564/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
        'id': '10001',
        'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
        'name': 'Story',
        'subtask': false
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-23T16:28:59.550+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-564/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-08T15:00:49.466+1100',
      'customfield_12200': null,
      'customfield_10022': 0.5,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06e8j:qv',
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-06T17:10:15.000+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"bitbucket":{"count":1,"name":"Bitbucket Cloud"}}}}},"isStale":true}}',
      'customfield_10018': 'MEGATRON-242',
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-23T16:28:59.556+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Update client/consumers of xflow service to use /cloudId/ endpoints instead of deprecated endpoints.',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_504790_*|*_10000_*:*_1_*:*_2512144652_*|*_12200_*:*_1_*:*_64995986_*|*_12400_*:*_1_*:*_1382826797_*|*_12600_*:*_2_*:*_5279',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114697',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114697',
    'key': 'MEGATRON-682',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 108703,
        'key': 'MEGATRON-242',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/108703',
        'name': 'Megatron Tech Debt',
        'summary': 'Bug fixes left over in Megatron work',
        'color': { 'key': 'color_9' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [{
        'id': '114771',
        'key': 'MEGATRON-687',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114771',
        'fields': {
          'summary': 'Fix broken xflow tests in GAB',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=lhunt',
        'name': 'lhunt',
        'key': 'lhunt',
        'accountId': '655363:5e0585ca-e27a-4d43-a22e-25ec4c932938',
        'emailAddress': 'lhunt@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/ce76d3cfa6c10fce9d6c19d58a50840e?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dlhunt%26avatarId%3D11002%26noRedirect%3Dtrue'
        },
        'displayName': 'Lachlan Hunt',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-682/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-23T16:53:59.179+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-682/watchers',
        'watchCount': 0,
        'isWatching': false
      },
      'created': '2018-02-13T11:23:58.432+1100',
      'customfield_12200': null,
      'customfield_10022': 5.0,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06e8j:qx',
      'customfield_13801': null,
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=4}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":4,"lastUpdated":"2018-02-22T10:49:33.742+1100","stateCount":4,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"stash":{"count":2,"name":"Bitbucket Server"},"bitbucket":{"count":2,"name":"Bitbucket Cloud"}}}}},"isStale":true}}',
      'customfield_10018': 'MEGATRON-242',
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-23T16:53:59.192+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': 'Bump versions in:\r\n* Jira\r\n* GAB\r\n* Home\r\n\r\nAlert Expandonauts of these changes.',
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Upgrade dependencies in atlaskit/xflow to ensure support for React 16',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_614132801_*|*_10000_*:*_1_*:*_106686_*|*_12200_*:*_1_*:*_248829712_*|*_10001_*:*_1_*:*_0_*|*_12600_*:*_1_*:*_20731566',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114771',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114771',
    'key': 'MEGATRON-687',
    'fields': {
      'parent': {
        'id': '114697',
        'key': 'MEGATRON-682',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114697',
        'fields': {
          'summary': 'Upgrade dependencies in atlaskit/xflow to ensure support for React 16',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
        'name': 'ahammond',
        'key': 'ahammond',
        'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
        'emailAddress': 'ahammond@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Hammond',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
        'name': 'ahammond',
        'key': 'ahammond',
        'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
        'emailAddress': 'ahammond@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Hammond',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
        'name': 'ahammond',
        'key': 'ahammond',
        'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
        'emailAddress': 'ahammond@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Hammond',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-687/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-22T10:52:46.837+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-687/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-21T15:44:31.942+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06fy7:',
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_13800': null,
      'customfield_11501': '{}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-22T10:52:46.845+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': null,
      'customfield_13000': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Fix broken xflow tests in GAB',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_4639508_*|*_10000_*:*_1_*:*_13363_*|*_12200_*:*_1_*:*_64242036_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114720',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114720',
    'key': 'MEGATRON-683',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 108703,
        'key': 'MEGATRON-242',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/108703',
        'name': 'Megatron Tech Debt',
        'summary': 'Bug fixes left over in Megatron work',
        'color': { 'key': 'color_9' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=awakeling',
        'name': 'awakeling',
        'key': 'awakeling',
        'accountId': '655363:bebdffcd-d1ee-4219-98a7-8b484a612112',
        'emailAddress': 'awakeling@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Wakeling',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=awakeling',
        'name': 'awakeling',
        'key': 'awakeling',
        'accountId': '655363:bebdffcd-d1ee-4219-98a7-8b484a612112',
        'emailAddress': 'awakeling@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Wakeling',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=awakeling',
        'name': 'awakeling',
        'key': 'awakeling',
        'accountId': '655363:bebdffcd-d1ee-4219-98a7-8b484a612112',
        'emailAddress': 'awakeling@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/69189eb9488007e129959741f7f1b8dc?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F69189eb9488007e129959741f7f1b8dc%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Wakeling',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-683/votes',
        'votes': 0,
        'hasVoted': false
      },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12200',
        'id': '12200',
        'key': 'MEGATRON',
        'name': 'Megatron Productionisation',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?pid=12200&avatarId=12600',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&pid=12200&avatarId=12600',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&pid=12200&avatarId=12600',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&pid=12200&avatarId=12600'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-19T16:11:42.945+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/MEGATRON-683/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-15T11:14:26.960+1100',
      'customfield_12200': null,
      'customfield_10022': 0.5,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06e8j:qy',
      'customfield_11502': null,
      'customfield_11501': '{}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_13803': null,
      'customfield_10018': 'MEGATRON-242',
      'customfield_13802': null,
      'updated': '2018-02-19T16:11:42.953+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'Test Project Pages on MS Edge and fix any issues.',
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Project Pages on MS Edge',
      'customfield_10000': null,
      'customfield_10001': '10000_*:*_1_*:*_363435998_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114725',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114725',
    'key': 'EUE-68',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 114545,
        'key': 'EUE-4',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114545',
        'name': 'Expose site admin to end users',
        'summary': 'Expose site admin to end users',
        'color': { 'key': 'color_9' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-68/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-20T10:25:12.555+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-68/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-15T15:37:07.338+1100',
      'customfield_12200': null,
      'customfield_10022': 0.5,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06f1m:',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_11501': '{repository={count=31, dataType=repository}, json={"cachedValue":{"errors":[],"summary":{"repository":{"overall":{"count":31,"lastUpdated":"2018-02-20T09:53:40.000+1100","dataType":"repository"},"byInstanceType":{"stash":{"count":31,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10018': 'EUE-4',
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-20T10:25:12.563+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'Pending design from [~htapia].',
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Flag when request access button is clicked',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_259222624_*|*_10000_*:*_1_*:*_70026652_*|*_12200_*:*_1_*:*_15523130_*|*_12400_*:*_1_*:*_68495657_*|*_10001_*:*_1_*:*_0_*|*_12600_*:*_1_*:*_17167',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114575',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114575',
    'key': 'EUE-31',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 114545,
        'key': 'EUE-4',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114545',
        'name': 'Expose site admin to end users',
        'summary': 'Expose site admin to end users',
        'color': { 'key': 'color_9' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': null,
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [{
        'id': '114744',
        'key': 'EUE-75',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114744',
        'fields': {
          'summary': "Site Administration appswitcher menu doesn't respond to click when flag is rendered",
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114745',
        'key': 'EUE-76',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114745',
        'fields': {
          'summary': 'Flag appears too quickly',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114746',
        'key': 'EUE-77',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114746',
        'fields': {
          'summary': 'Appswitcher menu stays open after dialog is rendered',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114747',
        'key': 'EUE-78',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114747',
        'fields': {
          'summary': 'Investigate rendering of multiple flags',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114748',
        'key': 'EUE-79',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114748',
        'fields': {
          'summary': 'Update copy on designs',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-31/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T14:58:39.153+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-31/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-31T14:45:21.339+1100',
      'customfield_12200': null,
      'customfield_10022': 0.5,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06f1y:i',
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{}',
      'customfield_13800': null,
      'customfield_10018': 'EUE-4',
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-21T14:58:39.160+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'Check copy is aligned with the recommendations on https://extranet.atlassian.com/display/TECHWRITING/2018/02/11/How+to+refer+to+products+and+apps+while+designing+and+developing+at+Atlassian%3A+A+DACI+story',
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Design QA',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_81949147_*|*_10000_*:*_1_*:*_1721221190_*|*_12200_*:*_2_*:*_11828911_*|*_10001_*:*_2_*:*_198579',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114744',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114744',
    'key': 'EUE-75',
    'fields': {
      'parent': {
        'id': '114575',
        'key': 'EUE-31',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114575',
        'fields': {
          'summary': 'Design QA',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-75/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T14:57:46.224+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-75/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-20T11:38:42.125+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06fsv:',
      'customfield_13800': null,
      'customfield_11501': '{repository={count=11, dataType=repository}, json={"cachedValue":{"errors":[],"summary":{"repository":{"overall":{"count":11,"lastUpdated":"2018-02-21T15:23:17.000+1100","dataType":"repository"},"byInstanceType":{"stash":{"count":11,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-21T14:57:46.233+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': "When the flag is already rendered, the appswitcher menu item for site admin doesn't respond to clicks. Expect people to retry, so may want to load dialog regardless.",
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': "Site Administration appswitcher menu doesn't respond to click when flag is rendered",
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_63773945_*|*_10000_*:*_1_*:*_9651313_*|*_12200_*:*_1_*:*_24918855_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114745',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114745',
    'key': 'EUE-76',
    'fields': {
      'parent': {
        'id': '114575',
        'key': 'EUE-31',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114575',
        'fields': {
          'summary': 'Design QA',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
        'name': 'dchukhai',
        'key': 'dchukhai',
        'accountId': '5a277860f55b675bba61419e',
        'emailAddress': 'dchukhai@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Dima Chukhai',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-76/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T10:03:43.318+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-76/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-20T11:39:49.399+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06ft3:',
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_11501': '{repository={count=9, dataType=repository}, json={"cachedValue":{"errors":[],"summary":{"repository":{"overall":{"count":9,"lastUpdated":"2018-02-21T11:14:55.000+1100","dataType":"repository"},"byInstanceType":{"stash":{"count":9,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-21T10:03:43.326+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': "The \"opps, didn't work\" flag renders very quickly. Users may feel like it wasn't real. Maybe add a small delay before rendering the flag, with a spinner on the dialog CTA.",
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Flag appears too quickly',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_8096068_*|*_10000_*:*_1_*:*_6612201_*|*_12200_*:*_1_*:*_65925661_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114748',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114748',
    'key': 'EUE-79',
    'fields': {
      'parent': {
        'id': '114575',
        'key': 'EUE-31',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114575',
        'fields': {
          'summary': 'Design QA',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-79/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T14:57:51.195+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-79/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-20T11:56:50.493+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06ft7:',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_11501': '{repository={count=4, dataType=repository}, json={"cachedValue":{"errors":[],"summary":{"repository":{"overall":{"count":4,"lastUpdated":"2018-02-21T15:23:17.000+1100","dataType":"repository"},"byInstanceType":{"stash":{"count":4,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-21T14:57:51.200+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': 'Some copy has to be updated (e.g. flag copy: remove please, etc).',
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Update copy on designs',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_66637977_*|*_10000_*:*_1_*:*_6075607_*|*_12200_*:*_1_*:*_24547126_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114746',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114746',
    'key': 'EUE-77',
    'fields': {
      'parent': {
        'id': '114575',
        'key': 'EUE-31',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114575',
        'fields': {
          'summary': 'Design QA',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': null,
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-77/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-20T14:03:26.107+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-77/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-20T11:42:01.243+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06ftb:',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{}',
      'customfield_13800': null,
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-20T14:03:26.114+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'When clicking on site-administrator, the dialog renders, but the app switcher menu is still rendered for a while.',
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Appswitcher menu stays open after dialog is rendered',
      'customfield_10000': null,
      'customfield_10001': '10000_*:*_1_*:*_8484878_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114746/comment/179756',
          'id': '179756',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "Won't do. The menu is obscured by the overlay.",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-20T14:02:59.080+1100',
          'updated': '2018-02-20T14:02:59.080+1100'
        }], 'maxResults': 1, 'total': 1, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114747',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114747',
    'key': 'EUE-78',
    'fields': {
      'parent': {
        'id': '114575',
        'key': 'EUE-31',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114575',
        'fields': {
          'summary': 'Design QA',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
        'name': 'dchukhai',
        'key': 'dchukhai',
        'accountId': '5a277860f55b675bba61419e',
        'emailAddress': 'dchukhai@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Dima Chukhai',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-78/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T10:03:22.203+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-78/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-02-20T11:44:55.011+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06ftj:',
      'customfield_11502': null,
      'customfield_13800': null,
      'customfield_11501': '{}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-21T10:03:22.209+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': 'We currently persist our flag until it is dismissed. Make sure it stacks as intended when working with other flags in the product.',
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [{
        'self': 'https://growth.jira-dev.com/rest/api/2/attachment/72854',
        'id': '72854',
        'filename': 'Screen Shot 2018-02-20 at 15.47.48.png',
        'author': {
          'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
          'name': 'dchukhai',
          'key': 'dchukhai',
          'accountId': '5a277860f55b675bba61419e',
          'emailAddress': 'dchukhai@atlassian.com',
          'avatarUrls': {
            '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
            '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
            '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
            '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
          },
          'displayName': 'Dima Chukhai',
          'active': true,
          'timeZone': 'Australia/Sydney'
        },
        'created': '2018-02-20T15:53:21.622+1100',
        'size': 68937,
        'mimeType': 'image/png',
        'content': 'https://growth.jira-dev.com/secure/attachment/72854/Screen+Shot+2018-02-20+at+15.47.48.png',
        'thumbnail': 'https://growth.jira-dev.com/secure/thumbnail/72854/Screen+Shot+2018-02-20+at+15.47.48.png'
      }, {
        'self': 'https://growth.jira-dev.com/rest/api/2/attachment/72855',
        'id': '72855',
        'filename': 'Screen Shot 2018-02-20 at 15.53.59.png',
        'author': {
          'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
          'name': 'dchukhai',
          'key': 'dchukhai',
          'accountId': '5a277860f55b675bba61419e',
          'emailAddress': 'dchukhai@atlassian.com',
          'avatarUrls': {
            '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
            '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
            '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
            '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
          },
          'displayName': 'Dima Chukhai',
          'active': true,
          'timeZone': 'Australia/Sydney'
        },
        'created': '2018-02-20T15:54:34.298+1100',
        'size': 51215,
        'mimeType': 'image/png',
        'content': 'https://growth.jira-dev.com/secure/attachment/72855/Screen+Shot+2018-02-20+at+15.53.59.png',
        'thumbnail': 'https://growth.jira-dev.com/secure/thumbnail/72855/Screen+Shot+2018-02-20+at+15.53.59.png'
      }],
      'flagged': false,
      'summary': 'Investigate rendering of multiple flags',
      'customfield_10000': '2018-02-20T15:55:06.485+1100',
      'customfield_10001': '3_*:*_1_*:*_1073851_*|*_10000_*:*_2_*:*_79233350_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114747/comment/179757',
          'id': '179757',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
            'name': 'dchukhai',
            'key': 'dchukhai',
            'accountId': '5a277860f55b675bba61419e',
            'emailAddress': 'dchukhai@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Dima Chukhai',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'This is how it looks when multiple flags are rendered \r\n !Screen Shot 2018-02-20 at 15.47.48.png|thumbnail! \r\nor  \r\n!Screen Shot 2018-02-20 at 15.53.59.png|thumbnail! \r\ndepending on flag order',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
            'name': 'dchukhai',
            'key': 'dchukhai',
            'accountId': '5a277860f55b675bba61419e',
            'emailAddress': 'dchukhai@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Dima Chukhai',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-20T15:55:06.485+1100',
          'updated': '2018-02-20T15:55:34.941+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114747/comment/179758',
          'id': '179758',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
            'name': 'dchukhai',
            'key': 'dchukhai',
            'accountId': '5a277860f55b675bba61419e',
            'emailAddress': 'dchukhai@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Dima Chukhai',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "Don't think we can do something until all apps use global \"flag service\" to render messages.",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
            'name': 'dchukhai',
            'key': 'dchukhai',
            'accountId': '5a277860f55b675bba61419e',
            'emailAddress': 'dchukhai@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Dima Chukhai',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-20T15:59:06.409+1100',
          'updated': '2018-02-20T15:59:06.409+1100'
        }], 'maxResults': 2, 'total': 2, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114576',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114576',
    'key': 'EUE-32',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 114545,
        'key': 'EUE-4',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114545',
        'name': 'Expose site admin to end users',
        'summary': 'Expose site admin to end users',
        'color': { 'key': 'color_9' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': null,
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [{
        'id': '114757',
        'key': 'EUE-80',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114757',
        'fields': {
          'summary': "Analytics events don't fire when using the keyboard",
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-32/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T14:12:14.050+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-32/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-31T14:45:26.844+1100',
      'customfield_12200': null,
      'customfield_10022': 0.5,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06f1y:r',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{}',
      'customfield_13800': null,
      'customfield_10018': 'EUE-4',
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-21T14:12:14.058+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'Analytic events listed here: https://extranet.atlassian.com/display/PGT/GROW-4976%3A+Allow+end+users+to+see+site+admin+link+and+request+access#experiment-areas--959801604',
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Analytics QA',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_14971361_*|*_10000_*:*_1_*:*_1797435858_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114757',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114757',
    'key': 'EUE-80',
    'fields': {
      'parent': {
        'id': '114576',
        'key': 'EUE-32',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114576',
        'fields': {
          'summary': 'Analytics QA',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
        'name': 'dchukhai',
        'key': 'dchukhai',
        'accountId': '5a277860f55b675bba61419e',
        'emailAddress': 'dchukhai@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Dima Chukhai',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
        'name': 'ahammond',
        'key': 'ahammond',
        'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
        'emailAddress': 'ahammond@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Hammond',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=ahammond',
        'name': 'ahammond',
        'key': 'ahammond',
        'accountId': '655363:75df2d18-76d4-4551-8786-e7686099f06d',
        'emailAddress': 'ahammond@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/9789227411cdefae4622dd3c2dafa54d?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Dahammond%26avatarId%3D11600%26noRedirect%3Dtrue'
        },
        'displayName': 'Andrew Hammond',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-80/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T14:11:48.545+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-80/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-02-21T10:15:39.423+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06fvj:',
      'customfield_11501': '{}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-21T14:11:48.553+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': null,
      'customfield_13000': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': "Analytics events don't fire when using the keyboard",
      'customfield_10000': '2018-02-21T14:11:42.535+1100',
      'customfield_10001': '3_*:*_1_*:*_1056802_*|*_10000_*:*_1_*:*_13112332_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114757/comment/179785',
          'id': '179785',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
            'name': 'dchukhai',
            'key': 'dchukhai',
            'accountId': '5a277860f55b675bba61419e',
            'emailAddress': 'dchukhai@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Dima Chukhai',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'I can observe events are being sent after hitting Enter key. False alarm.',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
            'name': 'dchukhai',
            'key': 'dchukhai',
            'accountId': '5a277860f55b675bba61419e',
            'emailAddress': 'dchukhai@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Dima Chukhai',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-21T14:11:42.535+1100',
          'updated': '2018-02-21T14:11:42.535+1100'
        }], 'maxResults': 1, 'total': 1, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114574',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114574',
    'key': 'EUE-30',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 114545,
        'key': 'EUE-4',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114545',
        'name': 'Expose site admin to end users',
        'summary': 'Expose site admin to end users',
        'color': { 'key': 'color_9' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': null,
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [{
        'id': '114743',
        'key': 'EUE-74',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114743',
        'fields': {
          'summary': 'Set up browser filtering',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114766',
        'key': 'EUE-81',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114766',
        'fields': {
          'summary': 'Check events are not being fired for site admins',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114767',
        'key': 'EUE-82',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114767',
        'fields': {
          'summary': 'Update analytic events prefix (include experiment key)',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-30/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T15:57:13.108+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-30/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-31T14:45:15.154+1100',
      'customfield_12200': null,
      'customfield_10022': 0.5,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06f1y:v',
      'customfield_13800': null,
      'customfield_11501': '{}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13803': null,
      'customfield_10018': 'EUE-4',
      'customfield_13802': null,
      'updated': '2018-02-21T15:57:14.737+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'QA + Bug fixing',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_21276308_*|*_10000_*:*_1_*:*_1797441659_*|*_10001_*:*_2_*:*_1621',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114743',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114743',
    'key': 'EUE-74',
    'fields': {
      'parent': {
        'id': '114574',
        'key': 'EUE-30',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114574',
        'fields': {
          'summary': 'QA + Bug fixing',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
        'name': 'dchukhai',
        'key': 'dchukhai',
        'accountId': '5a277860f55b675bba61419e',
        'emailAddress': 'dchukhai@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Dima Chukhai',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-74/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T15:57:06.406+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-74/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-20T11:09:03.256+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06fsn:',
      'customfield_13800': null,
      'customfield_11501': '{repository={count=7, dataType=repository}, json={"cachedValue":{"errors":[],"summary":{"repository":{"overall":{"count":7,"lastUpdated":"2018-02-22T08:51:28.000+1100","dataType":"repository"},"byInstanceType":{"stash":{"count":7,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-21T15:57:06.410+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'This experiment should only run on Chrome and Firefox.',
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Set up browser filtering',
      'customfield_10000': null,
      'customfield_10001': '10000_*:*_1_*:*_95347700_*|*_12200_*:*_1_*:*_8335458_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114766',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114766',
    'key': 'EUE-81',
    'fields': {
      'parent': {
        'id': '114574',
        'key': 'EUE-30',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114574',
        'fields': {
          'summary': 'QA + Bug fixing',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
        'name': 'dchukhai',
        'key': 'dchukhai',
        'accountId': '5a277860f55b675bba61419e',
        'emailAddress': 'dchukhai@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Dima Chukhai',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-81/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T15:57:08.609+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-81/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-21T14:42:55.963+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06fx3:',
      'customfield_11502': null,
      'customfield_13800': null,
      'customfield_11501': '{repository={count=9, dataType=repository}, json={"cachedValue":{"errors":[],"summary":{"repository":{"overall":{"count":9,"lastUpdated":"2018-02-22T11:35:48.000+1100","dataType":"repository"},"byInstanceType":{"stash":{"count":9,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-21T15:57:08.614+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Check events are not being fired for site admins',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_709388_*|*_10000_*:*_1_*:*_792688_*|*_12200_*:*_1_*:*_2950577_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114767',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114767',
    'key': 'EUE-82',
    'fields': {
      'parent': {
        'id': '114574',
        'key': 'EUE-30',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114574',
        'fields': {
          'summary': 'QA + Bug fixing',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-82/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T15:24:13.718+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-82/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-21T14:44:17.262+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06fxb:',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{repository={count=10, dataType=repository}, json={"cachedValue":{"errors":[],"summary":{"repository":{"overall":{"count":10,"lastUpdated":"2018-02-22T11:35:48.000+1100","dataType":"repository"},"byInstanceType":{"stash":{"count":10,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_13800': null,
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-21T15:24:13.724+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': null,
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Update analytic events prefix (include experiment key)',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_598224_*|*_10000_*:*_1_*:*_904931_*|*_12200_*:*_1_*:*_893311_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114573',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114573',
    'key': 'EUE-29',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 114545,
        'key': 'EUE-4',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114545',
        'name': 'Expose site admin to end users',
        'summary': 'Expose site admin to end users',
        'color': { 'key': 'color_9' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [{
        'id': '114734',
        'key': 'EUE-70',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114734',
        'fields': {
          'summary': 'Set up Redash query for monitoring',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114736',
        'key': 'EUE-71',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114736',
        'fields': {
          'summary': 'Update cohort picker with rollout percentage',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114738',
        'key': 'EUE-72',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114738',
        'fields': {
          'summary': 'Turn on LaunchDarkly FF in prod!',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114770',
        'key': 'EUE-84',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114770',
        'fields': {
          'summary': 'Bundle updated cohort picker to Jira Frontend',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-29/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-27T08:29:01.673+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-29/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-31T14:45:11.055+1100',
      'customfield_12200': null,
      'customfield_10022': 1.0,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06f1y:y',
      'customfield_11502': null,
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{}',
      'customfield_13803': null,
      'customfield_10018': 'EUE-4',
      'customfield_13802': null,
      'updated': '2018-02-27T08:29:01.677+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Deployment/healthchecking',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_430010841_*|*_10000_*:*_1_*:*_1880219785_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114734',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114734',
    'key': 'EUE-70',
    'fields': {
      'parent': {
        'id': '114573',
        'key': 'EUE-29',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114573',
        'fields': {
          'summary': 'Deployment/healthchecking',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-70/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-20T11:16:50.103+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-70/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-19T15:24:32.207+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06fr3:',
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_11501': '{}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-20T11:16:50.111+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Set up Redash query for monitoring',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_7298510_*|*_10000_*:*_1_*:*_64239398_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114734/comment/179750',
          'id': '179750',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
            'name': 'iflores',
            'key': 'iflores',
            'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
            'emailAddress': 'iflores@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
            },
            'displayName': 'Isabelle Flores',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'https://redash.data.internal.atlassian.com/queries/25099 (will set start date and filter out dev/staging instances once experiment launches)',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
            'name': 'iflores',
            'key': 'iflores',
            'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
            'emailAddress': 'iflores@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
            },
            'displayName': 'Isabelle Flores',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-20T10:19:01.453+1100',
          'updated': '2018-02-20T10:19:01.453+1100'
        }], 'maxResults': 1, 'total': 1, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114736',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114736',
    'key': 'EUE-71',
    'fields': {
      'parent': {
        'id': '114573',
        'key': 'EUE-29',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114573',
        'fields': {
          'summary': 'Deployment/healthchecking',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-71/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T15:40:53.307+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-71/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-02-19T15:26:33.757+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06frb:',
      'customfield_11502': null,
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-21T15:23:10.000+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"bitbucket":{"count":1,"name":"Bitbucket Cloud"}}}}},"isStale":true}}',
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-21T15:40:53.321+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'What percentage of instances do we want in control/variation for GROW-4976?',
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Update cohort picker with rollout percentage',
      'customfield_10000': '2018-02-20T16:03:27.416+1100',
      'customfield_10001': '3_*:*_1_*:*_3598428_*|*_10000_*:*_1_*:*_168782372_*|*_12200_*:*_1_*:*_1278768_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114736/comment/179754',
          'id': '179754',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
            'name': 'iflores',
            'key': 'iflores',
            'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
            'emailAddress': 'iflores@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
            },
            'displayName': 'Isabelle Flores',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': '[~gburrows] [~lbalan] [~lserna] can you give any insight to this? ',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
            'name': 'iflores',
            'key': 'iflores',
            'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
            'emailAddress': 'iflores@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
            },
            'displayName': 'Isabelle Flores',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-20T11:27:05.219+1100',
          'updated': '2018-02-20T11:27:05.219+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114736/comment/179759',
          'id': '179759',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=gburrows',
            'name': 'gburrows',
            'key': 'gburrows',
            'accountId': '655363:b2b676b2-702e-4750-81ff-de691e7a5319',
            'emailAddress': 'gburrows@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'George Burrows',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "For megatron, a 50% rollout of the appswitcher has been exposed to 13,000 unique end-users in the last week. Since this is a learning experiment, I think we should roll out to the first 25% of buckets to get half this number of exposures. We can run for longer than a week if we don't get interesting results.",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=gburrows',
            'name': 'gburrows',
            'key': 'gburrows',
            'accountId': '655363:b2b676b2-702e-4750-81ff-de691e7a5319',
            'emailAddress': 'gburrows@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'George Burrows',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-20T16:03:27.416+1100',
          'updated': '2018-02-20T16:03:27.416+1100'
        }], 'maxResults': 2, 'total': 2, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114738',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114738',
    'key': 'EUE-72',
    'fields': {
      'parent': {
        'id': '114573',
        'key': 'EUE-29',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114573',
        'fields': {
          'summary': 'Deployment/healthchecking',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-72/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-22T09:54:11.312+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-72/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-19T15:48:21.196+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06frr:',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_11501': '{}',
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-22T09:54:11.317+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'Update: [EUE experiments|https://app.launchdarkly.com/jira/production/features/eue.experiments/targeting]\r\n\r\nSee https://app.launchdarkly.com/jira/production/features/jira.software.pages.xflow/targeting for reference.\r\n\r\nAfterwards, run the experiment via Houston. This will allow us to track the start date/time in the experiment feature service. https://houston.us-east-1.prod.atl-paas.net/v3/experiments/EXPERIMENT-2192',
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Turn on LaunchDarkly FF in prod!',
      'customfield_10000': null,
      'customfield_10001': '3_*:*_1_*:*_3557023_*|*_10000_*:*_1_*:*_231264646_*|*_10001_*:*_1_*:*_0_*|*_12600_*:*_1_*:*_3128456',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114738/comment/179792',
          'id': '179792',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
            'name': 'iflores',
            'key': 'iflores',
            'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
            'emailAddress': 'iflores@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
            },
            'displayName': 'Isabelle Flores',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Confirmed that all code changes are in prod and working as expected.',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
            'name': 'iflores',
            'key': 'iflores',
            'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
            'emailAddress': 'iflores@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
            },
            'displayName': 'Isabelle Flores',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-22T08:02:20.521+1100',
          'updated': '2018-02-22T08:02:20.521+1100'
        }], 'maxResults': 1, 'total': 1, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114770',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114770',
    'key': 'EUE-84',
    'fields': {
      'parent': {
        'id': '114573',
        'key': 'EUE-29',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114573',
        'fields': {
          'summary': 'Deployment/healthchecking',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
            'id': '10002',
            'description': 'A task that needs to be done.',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
            'name': 'Task',
            'subtask': false,
            'avatarId': 10318
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-84/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-21T15:56:47.759+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-84/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-02-21T15:24:44.160+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06fxz:',
      'customfield_13801': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{repository={count=9, dataType=repository}, json={"cachedValue":{"errors":[],"summary":{"repository":{"overall":{"count":9,"lastUpdated":"2018-02-22T11:35:48.000+1100","dataType":"repository"},"byInstanceType":{"stash":{"count":9,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_13800': null,
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-21T15:56:47.765+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': 'Need to bundle version 4.4.142',
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Bundle updated cohort picker to Jira Frontend',
      'customfield_10000': null,
      'customfield_10001': '10000_*:*_1_*:*_378343_*|*_12200_*:*_1_*:*_1545266_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114547',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114547',
    'key': 'EUE-5',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 114563,
        'key': 'EUE-19',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114563',
        'name': 'Surface admin creation in invite user flow',
        'summary': 'Surface admin creation in the invite user flow',
        'color': { 'key': 'color_4' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/12600',
        'description': 'This status is managed internally by Jira Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Post-Deploy Verification',
        'id': '12600',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [{
        'id': '114549',
        'key': 'EUE-7',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114549',
        'fields': {
          'summary': 'Investigate feature flags in UM',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114550',
        'key': 'EUE-8',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114550',
        'fields': {
          'summary': 'Implement feature flag in UM',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114597',
        'key': 'EUE-48',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114597',
        'fields': {
          'summary': 'Create module for determining cohort from cohort picker and feature flag service',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/12600',
            'description': 'This status is managed internally by Jira Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Post-Deploy Verification',
            'id': '12600',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114598',
        'key': 'EUE-49',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114598',
        'fields': {
          'summary': 'Bring in @atlassiansox/experiment-cohort-picker to UM',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Done',
            'id': '10001',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
              'id': 3,
              'key': 'done',
              'colorName': 'green',
              'name': 'Done'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-5/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
        'id': '10001',
        'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
        'name': 'Story',
        'subtask': false
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-5/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-31T14:07:08.974+1100',
      'customfield_12200': null,
      'customfield_10022': 2.0,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06f1z:',
      'customfield_13800': null,
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-08T13:23:30.000+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"bitbucket":{"count":1,"name":"Bitbucket Cloud"}}}}},"isStale":true}}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_10018': 'EUE-19',
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-21T16:04:42.879+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Feature flags and cohort picker in UM',
      'customfield_10000': null,
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114549',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114549',
    'key': 'EUE-7',
    'fields': {
      'parent': {
        'id': '114547',
        'key': 'EUE-5',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114547',
        'fields': {
          'summary': 'Feature flags and cohort picker in UM',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/12600',
            'description': 'This status is managed internally by Jira Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Post-Deploy Verification',
            'id': '12600',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
            'id': '10001',
            'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
            'name': 'Story',
            'subtask': false
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-7/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-06T11:42:00.042+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-7/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-01-31T14:16:03.757+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06eq7:',
      'customfield_13801': null,
      'customfield_11501': '{}',
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-06T11:42:00.047+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'User management use feature flags for their rollouts (e.g. Auth0-SAML rollout https://extranet.atlassian.com/display/I/Auth0-SAML+Staged+rollout+plan)\r\n\r\nWe want to utilise the same feature flags for our upcoming experimentation',
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Investigate feature flags in UM',
      'customfield_10000': '2018-02-05T16:37:17.174+1100',
      'customfield_10001': '3_*:*_1_*:*_69486967_*|*_10000_*:*_1_*:*_439669327_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114549/comment/179646',
          'id': '179646',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': '*Feature flag documentation*\r\nhttps://extranet.atlassian.com/display/I/User+Management+Development+-+Getting+Started#UserManagementDevelopment-GettingStarted-Workingwithdarkfeatures\r\nNote: there are different flags for frontend vs backend\r\n\r\n*Launch Darkly*\r\nhttps://extranet.atlassian.com/display/BPP/How+to+deliver+a+new+Cloud+feature+using+LaunchDarkly\r\nProduction: https://app.launchdarkly.com/identity/production/features\r\nStaging: https://app.launchdarkly.com/identity/staging/features',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-05T16:37:17.174+1100',
          'updated': '2018-02-05T16:49:03.615+1100'
        }], 'maxResults': 1, 'total': 1, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114550',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114550',
    'key': 'EUE-8',
    'fields': {
      'parent': {
        'id': '114547',
        'key': 'EUE-5',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114547',
        'fields': {
          'summary': 'Feature flags and cohort picker in UM',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/12600',
            'description': 'This status is managed internally by Jira Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Post-Deploy Verification',
            'id': '12600',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
            'id': '10001',
            'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
            'name': 'Story',
            'subtask': false
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-8/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-07T10:02:22.632+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-8/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-01-31T14:16:13.595+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06eqf:',
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_11501': '{}',
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-07T10:02:22.638+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': "Steps required:\r\n* Create feature flag ticket if it hasn't been already created: https://extranet.atlassian.com/display/BPP/How+to+deliver+a+new+Cloud+feature+using+LaunchDarkly\r\n** Created https://extranet.atlassian.com/jira/browse/FD-1987\r\n* Create feature flag under Identity's launch darkly buckets tagged with FD ticket number.\r\n ([production|https://app.launchdarkly.com/identity/production/features], [staging|https://app.launchdarkly.com/identity/staging/features], [test|https://app.launchdarkly.com/identity/test/features])\r\n** May need to request access from Koopa Troopas team\r\n** Settings:\r\n{code}\r\nName: User Management End User Empowerment Experiments\r\nKey: identity.user_management.eue.experiments\r\nDescription: This flag controls end user empowerment experiments run by the Growth Team.\r\nTags: FD-1987\r\nDefault Rule: serve false\r\n{code}\r\n* Add feature flag in [feature-flags.js|https://stash.atlassian.com/projects/UN/repos/user-management/browse/unified-admin-chrome/src/main/resources/js/atlassian/feature-flags.js]\r\n* For soy templates, use the feature flag key directly.\r\n{code}\r\n{if isFeatureEnabled('test.eue')}\r\n  // ... logic\r\n{/if}\r\n{code}\r\n* Otherwise use adminApp.isFeatureEnabled(featureFlags.YOUR_FEATURE_FLAG).\r\n\r\nRelated: https://extranet.atlassian.com/display/I/User+Management+Development+-+Getting+Started#UserManagementDevelopment-GettingStarted-Workingwithdarkfeatures",
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Implement feature flag in UM',
      'customfield_10000': '2018-02-06T13:40:01.605+1100',
      'customfield_10001': '3_*:*_1_*:*_74401231_*|*_10000_*:*_1_*:*_515167816_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114550/comment/179674',
          'id': '179674',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Example of feature flags being removed in UM (DRS experiment)\r\nhttps://stash.atlassian.com/projects/UN/repos/user-management/commits/bbecf5e47e43f35988b71c820381116fa8f087b0',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-06T13:40:01.605+1100',
          'updated': '2018-02-06T13:40:01.605+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114550/comment/179675',
          'id': '179675',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Required for experiment: https://extranet.atlassian.com/display/PGT/GROW-4974%3A+First+X+users+during+evaluation+are+admins+by+default',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-06T14:18:41.008+1100',
          'updated': '2018-02-06T14:18:41.008+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114550/comment/179676',
          'id': '179676',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Waiting on permissions to modify identity feature flags:\r\nhttps://sdog.jira-dev.com/projects/DEQ/issue/DEQ-631',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-06T15:02:20.008+1100',
          'updated': '2018-02-06T15:11:18.134+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114550/comment/179684',
          'id': '179684',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Permissions granted. Closing issue since path is clear on how to add feature flags around each UI element as we build it.',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-07T10:02:04.482+1100',
          'updated': '2018-02-07T10:02:04.482+1100'
        }], 'maxResults': 4, 'total': 4, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114597',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114597',
    'key': 'EUE-48',
    'fields': {
      'parent': {
        'id': '114547',
        'key': 'EUE-5',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114547',
        'fields': {
          'summary': 'Feature flags and cohort picker in UM',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/12600',
            'description': 'This status is managed internally by Jira Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Post-Deploy Verification',
            'id': '12600',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
            'id': '10001',
            'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
            'name': 'Story',
            'subtask': false
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/12600',
        'description': 'This status is managed internally by Jira Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Post-Deploy Verification',
        'id': '12600',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-48/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-48/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-01-31T15:36:20.719+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06ezz:',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-14T15:01:41.841+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"stash":{"count":1,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-21T16:04:46.440+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': 'See [example|https://stash.atlassian.com/projects/JIRACLOUD/repos/jira-frontend/pull-requests/5954/diff] in Jira Frontend using LaunchDarkly (in plan sprint xflow touchpoint).',
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Create module for determining cohort from cohort picker and feature flag service',
      'customfield_10000': '2018-02-07T10:03:20.495+1100',
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114597/comment/179685',
          'id': '179685',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Feature flags created in UM in EUE-8\r\n\r\nPermissions to edit the feature flags in Launch darkly may require a ticket raised to be added to the Identity Team\r\nhttp://go/getdelenghelp',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-07T10:03:20.495+1100',
          'updated': '2018-02-07T10:04:51.819+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114597/comment/179699',
          'id': '179699',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Steps to get cohort-info module working in soy templates:\r\n* create get-cohort-info.js file that registers itself onto the window object\r\n* add desired functions to expose to soy templates in adminApp\r\n* create a SoyClientFunction that accesses the adminApp exposed function for cohort info\r\n* use the named SoyClientFunction created in desired soy templates',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-08T14:17:06.363+1100',
          'updated': '2018-02-08T14:17:06.363+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114597/comment/179713',
          'id': '179713',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Waiting on prod deployment:\r\nhttps://deployment-bamboo.internal.atlassian.com/deploy/viewDeploymentVersion.action?versionId=369919848\r\n\r\nUser management gets released weekly on Wednesday mornings.',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-14T16:49:47.978+1100',
          'updated': '2018-02-15T14:21:59.225+1100'
        }], 'maxResults': 3, 'total': 3, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114598',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114598',
    'key': 'EUE-49',
    'fields': {
      'parent': {
        'id': '114547',
        'key': 'EUE-5',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114547',
        'fields': {
          'summary': 'Feature flags and cohort picker in UM',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/12600',
            'description': 'This status is managed internally by Jira Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Post-Deploy Verification',
            'id': '12600',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
            'id': '10001',
            'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
            'name': 'Story',
            'subtask': false
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-49/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-08T13:23:58.741+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-49/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-01-31T15:48:48.502+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06f07:',
      'customfield_13801': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-08T13:23:51.039+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"stash":{"count":1,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_13800': null,
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-08T13:23:58.747+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Bring in @atlassiansox/experiment-cohort-picker to UM',
      'customfield_10000': '2018-02-06T15:31:40.585+1100',
      'customfield_10001': '3_*:*_1_*:*_94158200_*|*_10000_*:*_1_*:*_517273006_*|*_12200_*:*_1_*:*_71079043_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114598/comment/179678',
          'id': '179678',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Pending release of https://bitbucket.org/atlassian/experiment-js/pull-requests/249/eue-33-add-new-rollout-configuration-for/diff\r\n\r\n@atlassiansox/experiment-cohort-picker@4.4.139 released',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-06T15:31:40.585+1100',
          'updated': '2018-02-06T15:34:58.518+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114598/comment/179679',
          'id': '179679',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'crowd-user-management-plugin uses bower rather than webpack',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-06T16:00:31.383+1100',
          'updated': '2018-02-06T16:16:10.179+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114598/comment/179680',
          'id': '179680',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "Seems like there's an issue using cohort-picker in the jshint-um-test\r\n{code}\r\ncrowd-user-management-plugin/src/main/resources/js/lib/cohort-picker.js\r\n  line 76   col 14  Bad line breaking before '('.\r\n  line 266  col 32  'i' is already defined.\r\n  line 474  col 41  Use '===' to compare with '0'.\r\n  line 8    col 56  'exports' is not defined.\r\n  line 10   col 34  'self' is not defined.\r\n  line 85   col 58  'unescape' is not defined.\r\n  line 90   col 51  'escape' is not defined.\r\n\r\n  ⚠  7 warnings\r\n{code}\r\n\r\nMight need to investigate how we are building cohort picker.",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-06T17:07:39.720+1100',
          'updated': '2018-02-07T17:28:38.669+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114598/comment/179690',
          'id': '179690',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "Seems like external libraries are explicitly ignored in the jshint pipeline.\r\n\r\nProviding a library string in our webpack target output should make it available on the window object\r\n{code}\r\n  output: {\r\n    filename: 'cohortPicker.js',\r\n    path: path.join(__dirname, 'dist'),\r\n    library: 'CohortPicker',\r\n    libraryTarget: 'umd'\r\n  },\r\n{code}\r\n\r\nExposing an amd wrapper in UM should make it available in the UM frontend\r\n{code}\r\ndefine('cohort-picker', [], function() {\r\n    return window.CohortPicker;\r\n});\r\n\r\n{code}",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-07T17:31:39.407+1100',
          'updated': '2018-02-07T17:31:39.407+1100'
        }], 'maxResults': 4, 'total': 4, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114569',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114569',
    'key': 'EUE-25',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 114563,
        'key': 'EUE-19',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114563',
        'name': 'Surface admin creation in invite user flow',
        'summary': 'Surface admin creation in the invite user flow',
        'color': { 'key': 'color_4' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': null,
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
        'description': 'This status is managed internally by Jira Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Pending Release',
        'id': '12400',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [{
        'id': '114570',
        'key': 'EUE-26',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114570',
        'fields': {
          'summary': 'Add extra "site-admin" checkbox on main invite user screen',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
            'description': 'This status is managed internally by Jira Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Pending Release',
            'id': '12400',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114571',
        'key': 'EUE-27',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114571',
        'fields': {
          'summary': 'Add informative tooltip on main invite user screen',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
            'description': 'This status is managed internally by Jira Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Pending Release',
            'id': '12400',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-25/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
        'id': '10001',
        'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
        'name': 'Story',
        'subtask': false
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-25/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-31T14:35:45.105+1100',
      'customfield_12200': null,
      'customfield_10022': 1.0,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06f2f:',
      'customfield_13801': null,
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-19T13:51:12.561+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"stash":{"count":1,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13803': null,
      'customfield_10018': 'EUE-19',
      'customfield_13802': null,
      'updated': '2018-02-19T14:45:29.264+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Additional UI elements',
      'customfield_10000': null,
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114570',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114570',
    'key': 'EUE-26',
    'fields': {
      'parent': {
        'id': '114569',
        'key': 'EUE-25',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114569',
        'fields': {
          'summary': 'Additional UI elements',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
            'description': 'This status is managed internally by Jira Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Pending Release',
            'id': '12400',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
            'id': '10001',
            'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
            'name': 'Story',
            'subtask': false
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
        'name': 'dchukhai',
        'key': 'dchukhai',
        'accountId': '5a277860f55b675bba61419e',
        'emailAddress': 'dchukhai@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Dima Chukhai',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
        'description': 'This status is managed internally by Jira Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Pending Release',
        'id': '12400',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-26/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-26/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-31T14:36:11.494+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06eun:',
      'customfield_13801': null,
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-19T13:51:12.561+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"stash":{"count":1,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-19T14:45:33.226+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': '*Default:* unchecked',
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Add extra "site-admin" checkbox on main invite user screen',
      'customfield_10000': null,
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114571',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114571',
    'key': 'EUE-27',
    'fields': {
      'parent': {
        'id': '114569',
        'key': 'EUE-25',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114569',
        'fields': {
          'summary': 'Additional UI elements',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
            'description': 'This status is managed internally by Jira Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Pending Release',
            'id': '12400',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
            'id': '10001',
            'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
            'name': 'Story',
            'subtask': false
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
        'name': 'dchukhai',
        'key': 'dchukhai',
        'accountId': '5a277860f55b675bba61419e',
        'emailAddress': 'dchukhai@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Dima Chukhai',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
        'description': 'This status is managed internally by Jira Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Pending Release',
        'id': '12400',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'progress': { 'progress': 0, 'total': 0 },
      'customfield_11806': null,
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-27/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-27/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-31T14:36:30.728+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06euv:',
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-19T13:51:12.561+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"stash":{"count":1,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-19T14:45:36.270+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': null,
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': 'Add informative tooltip on main invite user screen',
      'customfield_10000': null,
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114733',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114733',
    'key': 'EUE-69',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': {
        'self': 'https://growth.jira-dev.com/rest/api/2/resolution/10000',
        'id': '10000',
        'description': 'Work has been completed on this issue.',
        'name': 'Done'
      },
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 114563,
        'key': 'EUE-19',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114563',
        'name': 'Surface admin creation in invite user flow',
        'summary': 'Surface admin creation in the invite user flow',
        'color': { 'key': 'color_4' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
        'name': 'dchukhai',
        'key': 'dchukhai',
        'accountId': '5a277860f55b675bba61419e',
        'emailAddress': 'dchukhai@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Dima Chukhai',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10001',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Done',
        'id': '10001',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/3',
          'id': 3,
          'key': 'done',
          'colorName': 'green',
          'name': 'Done'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-69/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': '2018-02-20T10:35:48.253+1100',
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-69/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-02-19T14:16:56.714+1100',
      'customfield_12200': null,
      'customfield_10022': 1.0,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06f2j:',
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_11501': '{}',
      'customfield_13800': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13803': null,
      'customfield_10018': 'EUE-19',
      'customfield_13802': null,
      'updated': '2018-02-20T10:35:48.258+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': 'Ticket to investigate how UM is currently firing analytic events.\r\n\r\nIt appears that javascript modules are utilising ```adminApp.triggerAnalytics``` to add to an event queue, that gets processed elsewhere (probably herment.js).\r\n\r\nConfirm sending analytics this way is correct, and is able to be queried/observed. May have to work with analysts to confirm.',
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Investigate firing analytics in user management',
      'customfield_10000': '2018-02-20T10:35:12.223+1100',
      'customfield_10001': '3_*:*_1_*:*_70950606_*|*_10000_*:*_1_*:*_2180941_*|*_10001_*:*_1_*:*_0',
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114733/comment/179751',
          'id': '179751',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
            'name': 'dchukhai',
            'key': 'dchukhai',
            'accountId': '5a277860f55b675bba61419e',
            'emailAddress': 'dchukhai@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Dima Chukhai',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Confirmed that `adminApp.triggerAnalytics()` is a correct way to send analytics. Posted events can be observed in Splunk an Redash',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
            'name': 'dchukhai',
            'key': 'dchukhai',
            'accountId': '5a277860f55b675bba61419e',
            'emailAddress': 'dchukhai@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Dima Chukhai',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-20T10:35:12.223+1100',
          'updated': '2018-02-20T10:35:12.223+1100'
        }], 'maxResults': 1, 'total': 1, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114572',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114572',
    'key': 'EUE-28',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 114563,
        'key': 'EUE-19',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114563',
        'name': 'Surface admin creation in invite user flow',
        'summary': 'Surface admin creation in the invite user flow',
        'color': { 'key': 'color_4' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=dchukhai',
        'name': 'dchukhai',
        'key': 'dchukhai',
        'accountId': '5a277860f55b675bba61419e',
        'emailAddress': 'dchukhai@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/05b0d14b8b59ad262f0beab9cf5f3dc3?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F05b0d14b8b59ad262f0beab9cf5f3dc3%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Dima Chukhai',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
        'description': 'This status is managed internally by Jira Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Pending Release',
        'id': '12400',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'closedSprints': [{
        'id': 166,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/166',
        'state': 'closed',
        'name': 'Megatron & EUE - HotChoc - 14',
        'startDate': '2018-01-31T05:22:30.513Z',
        'endDate': '2018-02-13T06:00:00.000Z',
        'completeDate': '2018-02-15T00:26:39.898Z',
        'originBoardId': 62,
        'goal': 'All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".'
      }],
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-28/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10002',
        'id': '10002',
        'description': 'A task that needs to be done.',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
        'name': 'Task',
        'subtask': false,
        'avatarId': 10318
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-28/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-31T14:39:01.573+1100',
      'customfield_12200': null,
      'customfield_10022': 0.5,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06f2r:',
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@6ca38c3[id=166,rapidViewId=62,state=CLOSED,name=Megatron & EUE - HotChoc - 14,goal=All major Megatron bugs are fixed.\nDev complete for "GROW-4974: First X users during evaluation are admins by default".,startDate=2018-01-31T05:22:30.513Z,endDate=2018-02-13T06:00:00.000Z,completeDate=2018-02-15T00:26:39.898Z,sequence=166]', 'com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_11501': '{}',
      'customfield_10018': 'EUE-19',
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-19T14:45:40.614+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': "Do the same as what's being used for the current Jira and Confluence checkboxes.",
      'customfield_13000': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Add user to "site-admin" group after creation based on checkbox value',
      'customfield_10000': null,
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114717',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114717',
    'key': 'EUE-63',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10903': null,
      'customfield_10749': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 114544,
        'key': 'EUE-3',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114544',
        'name': 'Admin default (first X users)',
        'summary': 'Surface admin creation in the invite user flow',
        'color': { 'key': 'color_7' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'timeestimate': null,
      'aggregatetimeoriginalestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
        'description': 'This status is managed internally by Jira Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Pending Release',
        'id': '12400',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=gburrows',
        'name': 'gburrows',
        'key': 'gburrows',
        'accountId': '655363:b2b676b2-702e-4750-81ff-de691e7a5319',
        'emailAddress': 'gburrows@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'George Burrows',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=gburrows',
        'name': 'gburrows',
        'key': 'gburrows',
        'accountId': '655363:b2b676b2-702e-4750-81ff-de691e7a5319',
        'emailAddress': 'gburrows@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/aa2d10ef818b2e62cec3d71d9c8326aa?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2Faa2d10ef818b2e62cec3d71d9c8326aa%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'George Burrows',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-63/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
        'id': '10001',
        'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
        'name': 'Story',
        'subtask': false
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-63/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-02-15T10:19:40.287+1100',
      'customfield_12200': null,
      'customfield_10022': 0.5,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06fkh:',
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-21T16:03:14.175+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"stash":{"count":1,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_13803': null,
      'customfield_10018': 'EUE-3',
      'customfield_13802': null,
      'updated': '2018-02-21T16:04:34.751+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': "Quick add is currently broken in prod because add-ons create too many system users during provisioning, meaning that the total number of users starts out above the Quick Add threshold. This bug is recorded here but it would be nice for us to fix it as part of this experiment:\r\n\r\nhttps://sdog.jira-dev.com/browse/KT-1011\r\n\r\nYou can see all the system users by changing the user filter to include System Users.\r\n\r\nSee https://stash.atlassian.com/projects/UN/repos/user-management/browse/crowd-user-management-plugin/src/main/resources/js/atlassian/routes/users.js#172\r\n\r\nIf we can't fix this in 1/2 a day, just increment the threshold for showing quick add (quick fix).",
      'customfield_13000': null,
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'customfield_10009': null,
      'attachment': [],
      'flagged': false,
      'summary': "Don't count system users as users when deciding whether or not to show quick add",
      'customfield_10000': '2018-02-16T14:21:53.170+1100',
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114717/comment/179729',
          'id': '179729',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'For reference: The system user filter uses lozenges to filter non-license users.\r\n\r\n{code}\r\n//Hide the user on the front end if they have a sysadmin or addon lozenge\r\n                List<LozengeEntity.Type> userLozenges = user.getLozenges().stream().map(LozengeEntity::getType).collect(CollectorsUtil.toImmutableList());\r\n                if (userLozenges.contains(LozengeEntity.Type.SYSADMIN) || userLozenges.contains(LozengeEntity.Type.ADDON_SYSADMIN)) {\r\n                    user.hideOnFrontEnd();\r\n                }\r\n                return user;\r\n{code}\r\n\r\ntable.tableContent returns the lozenges for each user, which can be used to filter out the system users (sysadmin and addon sysadmin)\r\n{code}\r\n    "lozenges": [\r\n      {\r\n        "name": "Sysadmin",\r\n        "title": "This user is an Atlassian system administrator",\r\n        "type": "sysadmin"\r\n      }\r\n    ],\r\n{code}',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-16T14:21:53.170+1100',
          'updated': '2018-02-16T14:21:53.170+1100'
        }], 'maxResults': 1, 'total': 1, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114561',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114561',
    'key': 'EUE-17',
    'fields': {
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': {
        'id': 114544,
        'key': 'EUE-3',
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/epic/114544',
        'name': 'Admin default (first X users)',
        'summary': 'Surface admin creation in the invite user flow',
        'color': { 'key': 'color_7' },
        'done': false
      },
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/10000',
        'description': '',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'To Do',
        'id': '10000',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/2',
          'id': 2,
          'key': 'new',
          'colorName': 'blue-gray',
          'name': 'To Do'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [{
        'id': '114593',
        'key': 'EUE-44',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114593',
        'fields': {
          'summary': 'Add site admin checkboxes in quick add view',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
            'description': 'This status is managed internally by Jira Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Pending Release',
            'id': '12400',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114594',
        'key': 'EUE-45',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114594',
        'fields': {
          'summary': 'Add value prop illustration and message to quick add view',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
            'description': 'This status is managed internally by Jira Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Pending Release',
            'id': '12400',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114595',
        'key': 'EUE-46',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114595',
        'fields': {
          'summary': 'Write logic to support randomisation between checked/unchecked by default',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
            'description': 'This status is managed internally by Jira Software',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'Pending Release',
            'id': '12400',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
              'id': 4,
              'key': 'indeterminate',
              'colorName': 'yellow',
              'name': 'In Progress'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114793',
        'key': 'EUE-90',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114793',
        'fields': {
          'summary': 'Add missing analytics',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10000',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'To Do',
            'id': '10000',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/2',
              'id': 2,
              'key': 'new',
              'colorName': 'blue-gray',
              'name': 'To Do'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }, {
        'id': '114802',
        'key': 'EUE-91',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114802',
        'fields': {
          'summary': 'Design QA',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10000',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'To Do',
            'id': '10000',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/2',
              'id': 2,
              'key': 'new',
              'colorName': 'blue-gray',
              'name': 'To Do'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
            'id': '10003',
            'description': 'The sub-task of the issue',
            'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
            'name': 'Sub-task',
            'subtask': true,
            'avatarId': 10316
          }
        }
      }],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-17/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
        'id': '10001',
        'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
        'name': 'Story',
        'subtask': false
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-17/watchers',
        'watchCount': 1,
        'isWatching': false
      },
      'created': '2018-01-31T14:32:09.117+1100',
      'customfield_12200': null,
      'customfield_10022': 2.0,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_10016': '0|i06fki:',
      'customfield_11502': null,
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-23T15:36:00.048+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"stash":{"count":1,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_13803': null,
      'customfield_10018': 'EUE-3',
      'customfield_13802': null,
      'updated': '2018-02-23T16:17:25.007+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'customfield_10007': null,
      'security': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Update quick add UI',
      'customfield_10000': null,
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': { 'comments': [], 'maxResults': 0, 'total': 0, 'startAt': 0 },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114593',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114593',
    'key': 'EUE-44',
    'fields': {
      'parent': {
        'id': '114561',
        'key': 'EUE-17',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114561',
        'fields': {
          'summary': 'Update quick add UI',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10000',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'To Do',
            'id': '10000',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/2',
              'id': 2,
              'key': 'new',
              'colorName': 'blue-gray',
              'name': 'To Do'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
            'id': '10001',
            'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
            'name': 'Story',
            'subtask': false
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
        'description': 'This status is managed internally by Jira Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Pending Release',
        'id': '12400',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'aggregatetimeestimate': null,
      'customfield_10724': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-44/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-44/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-01-31T15:23:07.815+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06ezb:',
      'customfield_13800': null,
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=1}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":1,"lastUpdated":"2018-02-23T15:36:00.048+1100","stateCount":1,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"stash":{"count":1,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-23T15:36:23.556+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'customfield_13000': null,
      'description': 'Design: https://app.zeplin.io/project/5a77884d42e12dda8b492aab/screen/5a77a2d4472780e08b7d5201',
      'customfield_13003': null,
      'customfield_10010': null,
      'customfield_10011': null,
      'customfield_13002': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Add site admin checkboxes in quick add view',
      'customfield_10000': '2018-02-19T13:48:05.330+1100',
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114593/comment/179739',
          'id': '179739',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "I'm presuming this includes the logic to add them to site-admins?",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-19T13:48:05.330+1100',
          'updated': '2018-02-19T13:48:05.330+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114593/comment/179741',
          'id': '179741',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
            'name': 'iflores',
            'key': 'iflores',
            'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
            'emailAddress': 'iflores@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
            },
            'displayName': 'Isabelle Flores',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'This particular task is to just add the checkboxes there, and https://growth.jira-dev.com/browse/EUE-6 is to actually implement the logic. Feel free to do both at the same time if it seems straightforward enough!',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
            'name': 'iflores',
            'key': 'iflores',
            'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
            'emailAddress': 'iflores@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
            },
            'displayName': 'Isabelle Flores',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-19T14:16:47.656+1100',
          'updated': '2018-02-19T14:16:47.656+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114593/comment/179742',
          'id': '179742',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'I might pick up EUE-6 as well in that case :) It would make testing the new checkboxes much easier.',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-19T14:19:19.592+1100',
          'updated': '2018-02-19T14:19:19.592+1100'
        }], 'maxResults': 3, 'total': 3, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114594',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114594',
    'key': 'EUE-45',
    'fields': {
      'parent': {
        'id': '114561',
        'key': 'EUE-17',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114561',
        'fields': {
          'summary': 'Update quick add UI',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10000',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'To Do',
            'id': '10000',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/2',
              'id': 2,
              'key': 'new',
              'colorName': 'blue-gray',
              'name': 'To Do'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
            'id': '10001',
            'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
            'name': 'Story',
            'subtask': false
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
        'description': 'This status is managed internally by Jira Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Pending Release',
        'id': '12400',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12100': null,
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-45/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-45/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-01-31T15:23:31.050+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10662': null,
      'customfield_10024': [],
      'customfield_12600': null,
      'customfield_10025': null,
      'customfield_10026': null,
      'customfield_10016': '0|i06ezj:',
      'customfield_11502': null,
      'customfield_13801': null,
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_13800': null,
      'customfield_11501': '{}',
      'customfield_10018': null,
      'customfield_13803': null,
      'customfield_13802': null,
      'updated': '2018-02-23T16:47:26.236+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': 'Design: https://app.zeplin.io/project/5a77884d42e12dda8b492aab/screen/5a77a2d4472780e08b7d5201',
      'customfield_13000': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_10012': null,
      'customfield_13005': null,
      'customfield_10013': null,
      'customfield_13004': null,
      'customfield_11500': null,
      'customfield_10015': null,
      'timetracking': {},
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Add value prop illustration and message to quick add view',
      'customfield_10000': '2018-02-20T11:24:52.859+1100',
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114594/comment/179753',
          'id': '179753',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': 'Assets uploaded to https://aes-artifacts--cdn.us-east-1.prod.public.atl-paas.net/hashed/6BrgnjJP1A1MZ7Z9p-aoltL0Mskxo1TnPIoyceGHUsc/meeple-scenes-professional.svg\r\n',
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-20T11:24:52.859+1100',
          'updated': '2018-02-20T11:24:52.859+1100'
        }], 'maxResults': 1, 'total': 1, 'startAt': 0
      },
      'customfield_10918': null
    }
  }, {
    'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
    'id': '114595',
    'self': 'https://growth.jira-dev.com/rest/agile/1.0/issue/114595',
    'key': 'EUE-46',
    'fields': {
      'parent': {
        'id': '114561',
        'key': 'EUE-17',
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/114561',
        'fields': {
          'summary': 'Update quick add UI',
          'status': {
            'self': 'https://growth.jira-dev.com/rest/api/2/status/10000',
            'description': '',
            'iconUrl': 'https://growth.jira-dev.com/',
            'name': 'To Do',
            'id': '10000',
            'statusCategory': {
              'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/2',
              'id': 2,
              'key': 'new',
              'colorName': 'blue-gray',
              'name': 'To Do'
            }
          },
          'priority': {
            'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
            'name': 'Medium',
            'id': '3'
          },
          'issuetype': {
            'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10001',
            'id': '10001',
            'description': 'Created by Jira Agile - do not edit or delete. Issue type for a user story.',
            'iconUrl': 'https://growth.jira-dev.com/images/icons/issuetypes/story.svg',
            'name': 'Story',
            'subtask': false
          }
        }
      },
      'fixVersions': [],
      'customfield_13500': null,
      'resolution': null,
      'customfield_12800': null,
      'customfield_10749': null,
      'customfield_10903': null,
      'customfield_10904': null,
      'customfield_10905': null,
      'lastViewed': null,
      'customfield_12000': 'If you mark a job as "stopped", it cannot be restarted (you must recreate the job). If you just want to temporarily disable this job, please use the "paused" status.',
      'epic': null,
      'priority': {
        'self': 'https://growth.jira-dev.com/rest/api/2/priority/3',
        'iconUrl': 'https://growth.jira-dev.com/images/icons/priorities/medium.svg',
        'name': 'Medium',
        'id': '3'
      },
      'labels': [],
      'aggregatetimeoriginalestimate': null,
      'timeestimate': null,
      'versions': [],
      'customfield_10738': null,
      'customfield_10739': null,
      'issuelinks': [],
      'assignee': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
        'name': 'mtruong',
        'key': 'mtruong',
        'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
        'emailAddress': 'mtruong@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
        },
        'displayName': 'Michael Truong',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'status': {
        'self': 'https://growth.jira-dev.com/rest/api/2/status/12400',
        'description': 'This status is managed internally by Jira Software',
        'iconUrl': 'https://growth.jira-dev.com/',
        'name': 'Pending Release',
        'id': '12400',
        'statusCategory': {
          'self': 'https://growth.jira-dev.com/rest/api/2/statuscategory/4',
          'id': 4,
          'key': 'indeterminate',
          'colorName': 'yellow',
          'name': 'In Progress'
        }
      },
      'components': [],
      'customfield_10724': null,
      'aggregatetimeestimate': null,
      'creator': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'subtasks': [],
      'reporter': {
        'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
        'name': 'iflores',
        'key': 'iflores',
        'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
        'emailAddress': 'iflores@atlassian.com',
        'avatarUrls': {
          '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
          '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
        },
        'displayName': 'Isabelle Flores',
        'active': true,
        'timeZone': 'Australia/Sydney'
      },
      'customfield_12100': null,
      'aggregateprogress': { 'progress': 0, 'total': 0 },
      'customfield_12500': null,
      'customfield_11402': null,
      'customfield_11801': null,
      'customfield_11800': null,
      'customfield_11803': null,
      'customfield_11802': null,
      'customfield_11805': null,
      'customfield_11804': null,
      'customfield_11806': null,
      'progress': { 'progress': 0, 'total': 0 },
      'votes': { 'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-46/votes', 'votes': 0, 'hasVoted': false },
      'worklog': { 'startAt': 0, 'maxResults': 20, 'total': 0, 'worklogs': [] },
      'issuetype': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issuetype/10003',
        'id': '10003',
        'description': 'The sub-task of the issue',
        'iconUrl': 'https://growth.jira-dev.com/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
        'name': 'Sub-task',
        'subtask': true,
        'avatarId': 10316
      },
      'timespent': null,
      'sprint': {
        'id': 167,
        'self': 'https://growth.jira-dev.com/rest/agile/1.0/sprint/167',
        'state': 'active',
        'name': 'Megatron & EUE-Irish Coffee-15',
        'startDate': '2018-02-15T04:55:56.833Z',
        'endDate': '2018-02-23T06:00:00.000Z',
        'originBoardId': 79,
        'goal': 'Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).'
      },
      'project': {
        'self': 'https://growth.jira-dev.com/rest/api/2/project/12902',
        'id': '12902',
        'key': 'EUE',
        'name': 'End User Empowerment',
        'projectTypeKey': 'software',
        'avatarUrls': {
          '48x48': 'https://growth.jira-dev.com/secure/projectavatar?avatarId=10324',
          '24x24': 'https://growth.jira-dev.com/secure/projectavatar?size=small&avatarId=10324',
          '16x16': 'https://growth.jira-dev.com/secure/projectavatar?size=xsmall&avatarId=10324',
          '32x32': 'https://growth.jira-dev.com/secure/projectavatar?size=medium&avatarId=10324'
        }
      },
      'customfield_13300': null,
      'aggregatetimespent': null,
      'customfield_11400': null,
      'resolutiondate': null,
      'workratio': -1,
      'watches': {
        'self': 'https://growth.jira-dev.com/rest/api/2/issue/EUE-46/watchers',
        'watchCount': 2,
        'isWatching': false
      },
      'created': '2018-01-31T15:23:59.202+1100',
      'customfield_12200': null,
      'customfield_10022': null,
      'customfield_10023': null,
      'customfield_10024': [],
      'customfield_10662': null,
      'customfield_10025': null,
      'customfield_12600': null,
      'customfield_10026': null,
      'customfield_13801': null,
      'customfield_11502': null,
      'customfield_10016': '0|i06ezr:',
      'customfield_10017': ['com.atlassian.greenhopper.service.sprint.Sprint@4c1fd081[id=167,rapidViewId=79,state=ACTIVE,name=Megatron & EUE-Irish Coffee-15,goal=Megatron is rolled out to at least 50%. Deploy the JFE experiment (request admin access).,startDate=2018-02-15T04:55:56.833Z,endDate=2018-02-23T06:00:00.000Z,completeDate=<null>,sequence=167]'],
      'customfield_11501': '{pullrequest={dataType=pullrequest, state=MERGED, stateCount=2}, json={"cachedValue":{"errors":[],"summary":{"pullrequest":{"overall":{"count":2,"lastUpdated":"2018-02-23T16:45:45.075+1100","stateCount":2,"state":"MERGED","dataType":"pullrequest","open":false},"byInstanceType":{"bitbucket":{"count":1,"name":"Bitbucket Cloud"},"stash":{"count":1,"name":"Bitbucket Server"}}}}},"isStale":true}}',
      'customfield_13800': null,
      'customfield_13803': null,
      'customfield_10018': null,
      'customfield_13802': null,
      'updated': '2018-02-23T15:36:24.686+1100',
      'timeoriginalestimate': null,
      'customfield_13001': null,
      'description': null,
      'customfield_13000': null,
      'customfield_10010': null,
      'customfield_13003': null,
      'customfield_13002': null,
      'customfield_10011': null,
      'customfield_13005': null,
      'customfield_10012': null,
      'customfield_13004': null,
      'customfield_10013': null,
      'customfield_11500': null,
      'timetracking': {},
      'customfield_10015': null,
      'customfield_10005': null,
      'customfield_10006': null,
      'security': null,
      'customfield_10007': null,
      'customfield_10008': null,
      'attachment': [],
      'customfield_10009': null,
      'flagged': false,
      'summary': 'Write logic to support randomisation between checked/unchecked by default',
      'customfield_10000': '2018-02-19T13:45:37.209+1100',
      'customfield_10001': null,
      'customfield_12300': null,
      'customfield_10004': null,
      'environment': null,
      'duedate': null,
      'comment': {
        'comments': [{
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114595/comment/179738',
          'id': '179738',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "Shouldn't cohort-picker already support this? It may require some modifications to how cohort-info is being used though.",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-19T13:45:37.209+1100',
          'updated': '2018-02-19T13:45:37.209+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114595/comment/179740',
          'id': '179740',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
            'name': 'iflores',
            'key': 'iflores',
            'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
            'emailAddress': 'iflores@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
            },
            'displayName': 'Isabelle Flores',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "The thought behind this was that it'd be easier to just randomise it on UM's side, and include relevant properties in the analytic events. If it's easier to adapt cohort picker to do this for us, we can go down that path instead :)",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=iflores',
            'name': 'iflores',
            'key': 'iflores',
            'accountId': '655363:87deebba-6c6c-4b3d-bf8a-9971c6b24983',
            'emailAddress': 'iflores@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=48&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3FownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=24&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=16&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dxsmall%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/580c497ac5a471f159b79e8f23e74974?s=32&d=https%3A%2F%2Fgrowth.jira-dev.com%2Fsecure%2Fuseravatar%3Fsize%3Dmedium%26ownerId%3Diflores%26avatarId%3D10700%26noRedirect%3Dtrue'
            },
            'displayName': 'Isabelle Flores',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-19T14:12:35.760+1100',
          'updated': '2018-02-19T14:12:35.760+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114595/comment/179802',
          'id': '179802',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "Let's randomise on UM's side (probably using the cloudId as the seed for consistency), and send randomised initial state in analytics.\r\n\r\nAdd analytics for checking/unchecking check boxes.",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-22T09:56:35.855+1100',
          'updated': '2018-02-22T09:56:35.855+1100'
        }, {
          'self': 'https://growth.jira-dev.com/rest/api/2/issue/114595/comment/179814',
          'id': '179814',
          'author': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'body': "After discussion with Dima, we will be modifying cohort-info in UM to handle multiple variations, and update cohort-picker to have two variations for 'eue.experiments'",
          'updateAuthor': {
            'self': 'https://growth.jira-dev.com/rest/api/2/user?username=mtruong',
            'name': 'mtruong',
            'key': 'mtruong',
            'accountId': '655362:c24f8bc9-60fa-4e38-a9c9-1575925d4ee2',
            'emailAddress': 'mtruong@atlassian.com',
            'avatarUrls': {
              '48x48': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=48&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D48%26noRedirect%3Dtrue',
              '24x24': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=24&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D24%26noRedirect%3Dtrue',
              '16x16': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=16&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D16%26noRedirect%3Dtrue',
              '32x32': 'https://d2b6kerldzk1uu.cloudfront.net/3c121264a9cda71719c918a26f6f6110?s=32&d=https%3A%2F%2Fsecure.gravatar.com%2Favatar%2F3c121264a9cda71719c918a26f6f6110%3Fd%3Dmm%26s%3D32%26noRedirect%3Dtrue'
            },
            'displayName': 'Michael Truong',
            'active': true,
            'timeZone': 'Australia/Sydney'
          },
          'created': '2018-02-22T13:17:01.921+1100',
          'updated': '2018-02-22T13:17:01.921+1100'
        }], 'maxResults': 4, 'total': 4, 'startAt': 0
      },
      'customfield_10918': null
    }
  }]
};

export default sprintIssues;
