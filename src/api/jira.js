/* global AP, MOCK */
import _ from 'lodash';

const getRequest = ({ url, type = 'GET' }) => {
  return new Promise((resolve, reject) => {
    //https://community.developer.atlassian.com/t/ap-require-usage-and-deprecation/5064/2
    AP.request({
      url,
      type,
      success: function(response) {
        response = JSON.parse(response);
        resolve(response);
      },
      error: function() {
        reject();
      }
    });
  });
};

const resolveAfterDelay = (data) => {
  return new Promise((resolve) => {
    _.delay(resolve, 300, data);
  });
};

export const getAllEpics = (boardId) => {
  if (MOCK) {
    const data = require('./mock-data/all-epics').default;
    console.log('using mock data for all epics', data);
    return resolveAfterDelay(data);
  }

  return getRequest({ url: `/rest/agile/1.0/board/${boardId}/epic` });
};

export const getAllIssuesForEpic = (epicId) => {
  if (MOCK) {
    const data = require('./mock-data/epic-issues').default;
    console.log('using mock data for sprint issues', data[epicId]);
    return resolveAfterDelay(data[epicId]);
  }

  return getRequest({ url: `/rest/agile/1.0/epic/${epicId}/issue` });
};

export const getAllIssuesForSprint = (boardId, sprintId) => {
  if (MOCK) {
    const data = require('./mock-data/sprint-issues').default;
    console.log('using mock data for sprint issues', data);
    return resolveAfterDelay(data);
  }

  return getRequest({ url: `/rest/agile/1.0/board/${boardId}/sprint/${sprintId}/issue` });
};

export const getAllSprints = (boardId) => {
  if (MOCK) {
    const data = require('./mock-data/all-sprints').default;
    console.log('using mock data for all sprints', data);
    return resolveAfterDelay(data);
  }

  return getRequest({ url: `/rest/agile/1.0/board/${boardId}/sprint` });
};

export const getProjectDetails = (projectId) => {
  if (MOCK) {
    const data = require('./mock-data/project').default;
    console.log('using mock data for project', data);
    return resolveAfterDelay(data);
  }

  return getRequest({ url: `/rest/api/2/project/${projectId}` });
};
