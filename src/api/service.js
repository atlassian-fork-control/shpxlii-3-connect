
const getRequest = ({ url }) => {
  return new Promise((resolve) => {

    fetch(url).then((response) => {
      // TODO handle error case
      resolve(response.json());
    });
  });
};

export const getAllEpics = (boardId, token) => {
  return getRequest({ url: `/epics?token=${token}&boardId=${boardId}` });
};

export const getAllIssuesForEpic = (epicId, token) => {
  return getRequest({ url: `/epic-issues?token=${token}&epicId=${epicId}` });
};

export const getProjectDetails = (projectId, token) => {
  return getRequest({ url: `/project-details?token=${token}&projectId=${projectId}` });
};
