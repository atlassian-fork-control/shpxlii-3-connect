import React from 'react';
import styled from 'styled-components';

import SprintName from './SprintName.jsx';
import SprintEnds from './SprintEnds.jsx';
import TeamMembers from './TeamMembers.jsx';

const SprintSummaryContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

class SprintSummary extends React.PureComponent {
    static propTypes = {
      ...SprintName.propTypes,
      ...SprintEnds.propTypes,
      ...TeamMembers.propTypes,
    }

    render() {
      const { sprintName, sprintEndDate, teamMembers } = this.props;

      return (
        <SprintSummaryContainer>
          <SprintName sprintName={sprintName}/>
          <TeamMembers teamMembers={teamMembers}/>
          <SprintEnds sprintEndDate={sprintEndDate}/>
        </SprintSummaryContainer>
      );
    }
}

export default SprintSummary;
