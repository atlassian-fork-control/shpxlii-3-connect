import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Name = styled.div`
    font-size: 29px;
    font-weight: bold;
    color: #172B4D;
    margin: 0 15px;
`;

class ProjectName extends React.PureComponent {
    static propTypes = {
      projectName: PropTypes.string.isRequired,
    }

    render() {
      return <Name>{this.props.projectName}</Name>;
    }
}

export default ProjectName;
