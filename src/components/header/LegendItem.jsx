import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const LegendItemContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin-right: 16px;
  align-items: center;
  margin-top: 16px;
  max-width: 300px;
`;

const LegendColorBox = styled.div`
  height: 28px;
  width: 28px;
  border: 1px solid #8993A4;
  border-radius: 4px;
  background-color: ${(props) => props.color};
`;

const LegendLabel = styled.div`
  color: #42526E;
  font-size: 18px;
  margin-left: 8px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

class LegendItem extends React.PureComponent {
    static propTypes = {
      color: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    }

    render() {
      const { color, label } = this.props;

      return (
        <LegendItemContainer>
          <LegendColorBox color={color}/>
          <LegendLabel>
            {label}
          </LegendLabel>
        </LegendItemContainer>
      );
    }
}

export default LegendItem;
