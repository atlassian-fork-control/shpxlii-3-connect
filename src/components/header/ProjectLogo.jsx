import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const ProjectAvatar = styled.div`
  height: 38px;
  width: 38px;
  background: url(${(props) => props.src}) no-repeat;
  background-size: cover;
`;

class ProjectLogo extends React.PureComponent {
    static propTypes = {
      projectAvatarSrc: PropTypes.string.isRequired,
    }

    render() {
      return <ProjectAvatar src={this.props.projectAvatarSrc}/>;
    }
}

export default ProjectLogo;
