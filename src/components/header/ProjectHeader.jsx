import React from 'react';
import styled from 'styled-components';

import OverallProgress from './OverallProgress.jsx';
import ProjectSummary from './ProjectSummary.jsx';

const HeaderContainer = styled.div`
   display: flex;
   flex-direction: column;
   justify-content: space-between;
   margin: 30px 0;
`;

class ProjectHeader extends React.PureComponent {
    static propTypes = {
      ...OverallProgress.propTypes,
      ...ProjectSummary.propTypes,
    }

    render() {
      const { projectAvatarSrc, projectName, teamMembers, epicCount, issueCount, legendItems, overallProgressData } = this.props;

      return (
        <HeaderContainer>
          <ProjectSummary
            projectAvatarSrc={projectAvatarSrc}
            projectName={projectName}
            teamMembers={teamMembers}
          />
          <OverallProgress
            epicCount={epicCount}
            issueCount={issueCount}
            legendItems={legendItems}
            overallProgressData={overallProgressData}
          />
        </HeaderContainer>
      );
    }
}

export default ProjectHeader;
