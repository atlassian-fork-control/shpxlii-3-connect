import React from 'react';
import styled from 'styled-components';

import Counters from './Counters.jsx';
import Legend from './Legend.jsx';
import BarGraph from '../common/BarGraph.jsx';

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 25px 0;
`;

const GraphWrapper = styled.div`
  height: 32px;
  margin-top: 16px;
  width: 100%;
`;

class OverallProgress extends React.PureComponent {
    static propTypes = {
      ...Counters.propTypes,
      ...Legend.propTypes,
      overallProgressData: BarGraph.propTypes.data,
    }

    render() {
      const { epicCount, issueCount, legendItems, overallProgressData } = this.props;

      return (
        <ColumnContainer>
          <Counters epicCount={epicCount} issueCount={issueCount}/>
          <GraphWrapper>
            <BarGraph
              data={overallProgressData}
              size="large"
              cutoff={1}
            />
          </GraphWrapper>
          <Legend legendItems={legendItems}/>
        </ColumnContainer>
      );
    }
}

export default OverallProgress;
