import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Tooltip from '@atlaskit/tooltip';
import { getHost } from '../../utils';

const WrappedTooltip = styled(Tooltip)`
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  flex: 1;
`;

const Link = styled.a`
  font-size: 14px;
  color: #000000;
  margin-left: 8px;
  &:hover {
    color: rgba(0,0,0,0.8);
  }
`;

class TicketText extends React.PureComponent {
    static propTypes = {
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    }

    render() {
      const { id, name } = this.props;
      const href = `${getHost()}/browse/${id}`;

      return (
        <WrappedTooltip content={name}>
          <Link href={href} target="_blank">{name}</Link>
        </WrappedTooltip>
      );
    }
};

export default TicketText;
